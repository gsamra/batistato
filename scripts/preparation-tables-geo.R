
#  ------------------------------------------------------------------------
#
# Titre : Preparation tables geo
#  Desc : Table contenant les contours géographiques des communes, departements et EPCI
#  Date : 2023-01-09
#
#  ------------------------------------------------------------------------



# Packages ----------------------------------------------------------------

library(data.table)
library(sf)
library(leaflet)
library(dplyr)



# Preparation data --------------------------------------------------------

### COMMUNES
communes <- fread("inputs/L_COMMUNE_ARRDT_EPCI_EPT_BDT_S_R11_2020.csv")
wkb <- structure(as.list(communes$geom), class = "WKB")
communes[, geom := st_as_sfc(wkb, EWKB = TRUE, crs = 2154)]
communes <- communes %>% 
  st_as_sf() %>% # conversion en objet sf (format geospatial R)
  st_transform(crs = 4326) %>% # Changement projection
  mutate(code_insee = as.character(code_insee))
communes


### EPCI
epci <- fread("inputs/L_EPCI_EPT_BDT_S_R11_2020.csv", encoding = "UTF-8")
wkb <- structure(as.list(epci$geom), class = "WKB")
epci[, geom := st_as_sfc(wkb, EWKB = TRUE, crs = 2154)]
epci <- epci %>% 
  st_as_sf() %>% # conversion en objet sf (format geospatial R)
  st_transform(crs = 4326) %>% # Changement projection
  mutate(id_ept_epci = as.character(id_ept_epci))
epci


### DEPARTEMENTS
departements <- fread("inputs/L_DEP_BDT_S_R11.csv")
wkb <- structure(as.list(departements$geom), class = "WKB")
departements[, geom := st_as_sfc(wkb, EWKB = TRUE, crs = 2154)]
departements <- departements %>% 
  st_as_sf() %>% # conversion en objet sf (format geospatial R)
  st_transform(crs = 4326) %>% # Changement projection
  mutate(insee_dep = as.character(insee_dep))
departements


### REGION
region <- departements %>% 
  mutate(region = "Île-de-France") %>% 
  group_by(region) %>% 
  summarise() %>% 
  ungroup()
region




# Test affichage dans leaflet -----------------------------------------------------------------------------------------------------------------------------


if (FALSE) { # ne pas exécuter lorsque l'on source le script
  
  leaflet(communes) %>%
    addTiles() %>%
    addPolygons()
  
  leaflet(epci) %>%
    addTiles() %>%
    addPolygons()
  
  leaflet(departements) %>%
    addTiles() %>%
    addPolygons()
  
  
  leaflet(region) %>%
    addTiles() %>%
    addPolygons()
  
}


# Sauvegarde format R -----------------------------------------------------

saveRDS(communes, file = "datas/communes.rds")
saveRDS(epci, file = "datas/epci.rds")
saveRDS(departements, file = "datas/departements.rds")
saveRDS(region, file = "datas/region.rds")
