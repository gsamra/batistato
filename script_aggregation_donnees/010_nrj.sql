DO
$$
BEGIN

/* Le nom de la table finale table_communes_insee est à modifier dans tout le code */
/* Le nombre de la table d'origine table_origine_energie est à modifier dans tout le code */
/* Le nom de la table finale table_finale_energie est à modifier dans tout le code */


	-- 1. CREATION DE LA TABLE
/*
		-- supprimer la table si elle existe
		drop table if exists table_finale_energie;

		-- créer la table 
		create table table_finale_energie (
			maille		varchar(8) NOT NULL,
			idcom 		varchar(5) ,
			siren_epci	varchar(254),
			dep_com		varchar(254),
			secteur		varchar(10),
			type_usage	varchar(10),
			type_bati	varchar(10),
			energie		varchar(4),
			conso		numeric,
			ges			numeric
		);
*/
	-- 2. CALCUL DES DONNEES NIVEAU COMMUNE
		-- supprimer la table temporaire si elle existe
		drop table if exists t_b_nrj_0;

		-- créer la table temporaire avec uniquement les variables nécessaires
		create temporary table t_b_nrj_0
		as 
		select insee_com as idcom,CONSO_AGRI,CONSO_IND,CONSO_TR,
		CONSO_RES_ECS,
		CONSO_RES_AUTRE,
		CONSO_APPT_CHF_BOIS,
		CONSO_APPT_CHF_CU,
		CONSO_APPT_CHF_ELEC,
		CONSO_APPT_CHF_GN,
		CONSO_APPT_CHF_PP,
		CONSO_MI_CHF_BOIS,
		CONSO_MI_CHF_ELEC,
		CONSO_MI_CHF_GN,
		CONSO_MI_CHF_PP,
		BRX_AUTRES_CU,
		BRX_AUTRES_ELEC,
		BRX_AUTRES_GN,
		BRX_AUTRES_PP,
		BRX_CHF_BOIS,
		BRX_CHF_CU,
		BRX_CHF_ELEC,
		BRX_CHF_GN,
		BRX_CHF_PP,
		BRX_ECS_CU,
		BRX_ECS_ELEC,
		BRX_ECS_GN,
		BRX_ECS_PP,
		CAHORE_AUTRES_CU,
		CAHORE_AUTRES_ELEC,
		CAHORE_AUTRES_GN,
		CAHORE_AUTRES_PP,
		CAHORE_CHF_CU,
		CAHORE_CHF_ELEC,
		CAHORE_CHF_GN,
		CAHORE_CHF_PP,
		CAHORE_ECS_CU,
		CAHORE_ECS_ELEC,
		CAHORE_ECS_GN,
		CAHORE_ECS_PP,
		COM_AUTRES_CU,
		COM_AUTRES_ELEC,
		COM_AUTRES_GN,
		COM_AUTRES_PP,
		COM_CHF_CU,
		COM_CHF_ELEC,
		COM_CHF_GN,
		COM_CHF_PP,
		COM_ECS_CU,
		COM_ECS_ELEC,
		COM_ECS_GN,
		COM_ECS_PP,
		HABCOM_AUTRES_CU,
		HABCOM_AUTRES_ELEC,
		HABCOM_AUTRES_GN,
		HABCOM_AUTRES_PP,
		HABCOM_CHF_CU,
		HABCOM_CHF_ELEC,
		HABCOM_CHF_GN,
		HABCOM_CHF_PP,
		HABCOM_ECS_CU,
		HABCOM_ECS_ELEC,
		HABCOM_ECS_GN,
		HABCOM_ECS_PP,
		ENSGT_AUTRES_CU,
		ENSGT_AUTRES_ELEC,
		ENSGT_AUTRES_GN,
		ENSGT_AUTRES_PP,
		ENSGT_CHF_CU,
		ENSGT_CHF_ELEC,
		ENSGT_CHF_GN,
		ENSGT_CHF_PP,
		ENSGT_ECS_CU,
		ENSGT_ECS_ELEC,
		ENSGT_ECS_GN,
		ENSGT_ECS_PP,
		SANTESOC_AUTRES_CU,
		SANTESOC_AUTRES_ELEC,
		SANTESOC_AUTRES_GN,
		SANTESOC_AUTRES_PP,
		SANTESOC_CHF_CU,
		SANTESOC_CHF_ELEC,
		SANTESOC_CHF_GN,
		SANTESOC_CHF_PP,
		SANTESOC_ECS_CU,
		SANTESOC_ECS_ELEC,
		SANTESOC_ECS_GN,
		SANTESOC_ECS_PP,
		SPORTLOIS_AUTRES_CU,
		SPORTLOIS_AUTRES_ELEC,
		SPORTLOIS_AUTRES_GN,
		SPORTLOIS_CHF_CU,
		SPORTLOIS_CHF_ELEC,
		SPORTLOIS_CHF_GN,
		SPORTLOIS_CHF_PP,
		SPORTLOIS_ECS_CU,
		SPORTLOIS_ECS_ELEC,
		SPORTLOIS_ECS_GN,
		SPORTLOIS_ECS_PP,
		TR_AUTRES_CU,
		TR_AUTRES_ELEC,
		TR_AUTRES_GN,
		TR_AUTRES_PP,
		TR_CHF_CU,
		TR_CHF_ELEC,
		TR_CHF_GN,
		TR_CHF_PP,
		TR_ECS_CU,
		TR_ECS_ELEC,
		TR_ECS_GN,
		TR_ECS_PP,
		GES_SCOPE12_RES,
		GES_SCOPE12_TER
		from table_origine_energie
		;

		-- secteur,usage,type
		drop table if exists t_b_nrj_co;
		create temporary table t_b_nrj_co
		as 
		select idcom,'IND' as Secteur,NULL as type_usage,NULL as type_bati,NULL as energie,CONSO_IND as Consommation
		from t_b_nrj_0
			union
		select idcom,'AGRI' as Secteur,NULL as type_usage,NULL as type_bati,NULL as energie,CONSO_AGRI as Consommation
		from t_b_nrj_0	
			union
		select idcom,'TR' as Secteur,NULL as type_usage,NULL as type_bati,NULL as energie,CONSO_TR as Consommation
		from t_b_nrj_0	
			union
		select idcom,'RES' as Secteur,'ECS' as type_usage,NULL as type_bati,NULL as energie,CONSO_RES_ECS as Consommation
		from t_b_nrj_0	
			union
		select idcom,'RES' as Secteur,'AUTRE' as type_usage,NULL as type_bati,NULL as energie,CONSO_RES_AUTRE as Consommation
		from t_b_nrj_0	
			union
		select idcom,'RES' as Secteur,'CHF' as type_usage,'APPT' as type_bati,'BOIS' as energie,CONSO_APPT_CHF_BOIS as Consommation
		from t_b_nrj_0
			union
		select idcom,'RES' as Secteur,'CHF' as type_usage,'APPT' as type_bati,'CU' as energie,CONSO_APPT_CHF_CU as Consommation
		from t_b_nrj_0
			union
		select idcom,'RES' as Secteur,'CHF' as type_usage,'APPT' as type_bati,'ELEC' as energie,CONSO_APPT_CHF_ELEC as Consommation
		from t_b_nrj_0
			union
		select idcom,'RES' as Secteur,'CHF' as type_usage,'APPT' as type_bati,'GN' as energie,CONSO_APPT_CHF_GN as Consommation
		from t_b_nrj_0
			union
		select idcom,'RES' as Secteur,'CHF' as type_usage,'APPT' as type_bati,'PP' as energie,CONSO_APPT_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'RES' as Secteur,'CHF' as type_usage,'MI' as type_bati,'BOIS' as energie,CONSO_MI_CHF_BOIS as Consommation
		from t_b_nrj_0
			union
		select idcom,'RES' as Secteur,'CHF' as type_usage,'MI' as type_bati,'ELEC' as energie,CONSO_MI_CHF_ELEC as Consommation
		from t_b_nrj_0
			union
		select idcom,'RES' as Secteur,'CHF' as type_usage,'MI' as type_bati,'GN' as energie,CONSO_MI_CHF_GN as Consommation
		from t_b_nrj_0
			union
		select idcom,'RES' as Secteur,'CHF' as type_usage,'MI' as type_bati,'PP' as energie,CONSO_MI_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'AUTRES' as type_usage,'BRX' as type_bati,NULL as energie,BRX_AUTRES_CU+BRX_AUTRES_ELEC+BRX_AUTRES_GN+BRX_AUTRES_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'CHF' as type_usage,'BRX' as type_bati,NULL as energie,BRX_CHF_BOIS+BRX_CHF_CU+BRX_CHF_ELEC+BRX_CHF_GN+BRX_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'ECS' as type_usage,'BRX' as type_bati,NULL as energie,BRX_ECS_CU+BRX_ECS_ELEC+BRX_ECS_GN+BRX_ECS_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'AUTRES' as type_usage,'CAHORE' as type_bati,NULL as energie,CAHORE_AUTRES_CU+CAHORE_AUTRES_ELEC+CAHORE_AUTRES_GN+CAHORE_AUTRES_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'CHF' as type_usage,'CAHORE' as type_bati,NULL as energie,CAHORE_CHF_CU+CAHORE_CHF_ELEC+CAHORE_CHF_GN+CAHORE_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'ECS' as type_usage,'CAHORE' as type_bati,NULL as energie,CAHORE_ECS_CU+CAHORE_ECS_ELEC+CAHORE_ECS_GN+CAHORE_ECS_PP as Consommation
		from t_b_nrj_0	
			union
		select idcom,'TER' as Secteur,'AUTRES' as type_usage,'COM' as type_bati,NULL as energie,COM_AUTRES_CU+COM_AUTRES_ELEC+COM_AUTRES_GN+COM_AUTRES_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'CHF' as type_usage,'COM' as type_bati,NULL as energie,COM_CHF_CU+COM_CHF_ELEC+COM_CHF_GN+COM_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'ECS' as type_usage,'COM' as type_bati,NULL as energie,COM_ECS_CU+COM_ECS_ELEC+COM_ECS_GN+COM_ECS_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'AUTRES' as type_usage,'HABCOM' as type_bati,NULL as energie,HABCOM_AUTRES_CU+HABCOM_AUTRES_ELEC+HABCOM_AUTRES_GN+HABCOM_AUTRES_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'CHF' as type_usage,'HABCOM' as type_bati,NULL as energie,HABCOM_CHF_CU+HABCOM_CHF_ELEC+HABCOM_CHF_GN+HABCOM_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'ECS' as type_usage,'HABCOM' as type_bati,NULL as energie,HABCOM_ECS_CU+HABCOM_ECS_ELEC+HABCOM_ECS_GN+HABCOM_ECS_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'AUTRES' as type_usage,'ENSGT' as type_bati,NULL as energie,ENSGT_AUTRES_CU+ENSGT_AUTRES_ELEC+ENSGT_AUTRES_GN+ENSGT_AUTRES_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'CHF' as type_usage,'ENSGT' as type_bati,NULL as energie,ENSGT_CHF_CU+ENSGT_CHF_ELEC+ENSGT_CHF_GN+ENSGT_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'ECS' as type_usage,'ENSGT' as type_bati,NULL as energie,ENSGT_ECS_CU+ENSGT_ECS_ELEC+ENSGT_ECS_GN+ENSGT_ECS_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'AUTRES' as type_usage,'SANTESOC' as type_bati,NULL as energie,SANTESOC_AUTRES_CU+SANTESOC_AUTRES_ELEC+SANTESOC_AUTRES_GN+SANTESOC_AUTRES_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'CHF' as type_usage,'SANTESOC' as type_bati,NULL as energie,SANTESOC_CHF_CU+SANTESOC_CHF_ELEC+SANTESOC_CHF_GN+SANTESOC_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'ECS' as type_usage,'SANTESOC' as type_bati,NULL as energie,SANTESOC_ECS_CU+SANTESOC_ECS_ELEC+SANTESOC_ECS_GN+SANTESOC_ECS_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'AUTRES' as type_usage,'SPORTLOIS' as type_bati,NULL as energie,SPORTLOIS_AUTRES_CU+SPORTLOIS_AUTRES_ELEC+SPORTLOIS_AUTRES_GN as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'CHF' as type_usage,'SPORTLOIS' as type_bati,NULL as energie,SPORTLOIS_CHF_CU+SPORTLOIS_CHF_ELEC+SPORTLOIS_CHF_GN+SPORTLOIS_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'ECS' as type_usage,'SPORTLOIS' as type_bati,NULL as energie,SPORTLOIS_ECS_CU+SPORTLOIS_ECS_ELEC+SPORTLOIS_ECS_GN+SPORTLOIS_ECS_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'AUTRES' as type_usage,'TR' as type_bati,NULL as energie,TR_AUTRES_CU+TR_AUTRES_ELEC+TR_AUTRES_GN+TR_AUTRES_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'CHF' as type_usage,'TR' as type_bati,NULL as energie,TR_CHF_CU+TR_CHF_ELEC+TR_CHF_GN+TR_CHF_PP as Consommation
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,'ECS' as type_usage,'TR' as type_bati,NULL as energie,TR_ECS_CU+TR_ECS_ELEC+TR_ECS_GN+TR_ECS_PP as Consommation
		from t_b_nrj_0
		;

		drop table if exists t_b_nrj_ges;
		create temporary table t_b_nrj_ges
		as
		select idcom,'RES' as Secteur,GES_SCOPE12_RES as ges
		from t_b_nrj_0
			union
		select idcom,'TER' as Secteur,GES_SCOPE12_TER as ges
		from t_b_nrj_0
		;
	
		truncate table_finale_energie;
		insert into table_finale_energie
		select 'COMMUNE' as maille,idcom,NULL as siren_epci,null as dep_com,
		Secteur,type_usage,type_bati,energie,Consommation as conso
		from t_b_nrj_co;

		insert into table_finale_energie
		select 'COMMUNE' as maille,idcom,NULL as siren_epci,null as dep_com,
		Secteur,NULL as type_usage,NULL as type_bati,NULL as energie,NULL as conso,ges
		from t_b_nrj_ges;

	-- 3. GROUPEMENT PAR EPCI, EPT, DEPARTEMENT
		DELETE 
		FROM table_finale_energie 
		WHERE maille<>'COMMUNE';

		/*Communes et leurs EPCI hors département 28*/
		drop table if exists t_communes;
		create temporary table t_communes
		as select code_insee,siren_epci  -- EPCI
		from table_communes_insee
		where dep_com<>'28' and siren_epci is not null
		union
		select code_insee,id_ept as siren_epci -- EPT
		from table_communes_insee
		where dep_com<>'28' and id_ept is not null
		; -- CC du pays Houdanais, Région Ile de France et département 28

		drop table if exists t_dep;
		create temporary table t_dep
		as select code_insee,dep_com 
		from table_communes_insee
		where dep_com<>'28' 
		;

		drop table if exists t_bat_1;
		create temporary table t_bat_1
		as select 'EPCI' as maille, c.siren_epci as siren_epci,
		secteur,type_usage,type_bati,energie,sum(conso) as conso,sum(ges) as ges
		from table_finale_energie b_0 
			left join t_communes c
				on b_0.idcom=c.code_insee
		group by maille,c.siren_epci,secteur,type_usage,type_bati,energie
		;

		drop table if exists t_bat_2;
		create temporary table t_bat_2
		as select 'DEP' as maille, c.dep_com as dep_com,
		secteur,type_usage,type_bati,energie,sum(conso) as conso,sum(ges) as ges
		from table_finale_energie b_0 
			inner join t_dep c
				on b_0.idcom=c.code_insee
		group by maille,c.dep_com,secteur,type_usage,type_bati,energie;

		drop table if exists t_bat_3;
		create temporary table t_bat_3
		as select 'REGION' as maille, 
		secteur,type_usage,type_bati,energie,sum(conso) as conso,sum(ges) as ges
		from table_finale_energie b_t 
		group by maille,secteur,type_usage,type_bati,energie;

		insert into table_finale_energie
		select maille,NULL as idcom,siren_epci,NULL as dep_com,secteur,type_usage,type_bati,energie,conso,ges from t_bat_1;

		insert into table_finale_energie
		select maille,NULL as idcom,NULL as siren_epci,dep_com,secteur,type_usage,type_bati,energie,conso,ges  from t_bat_2;

		insert into table_finale_energie
		select maille,NULL as idcom,NULL as siren_epci,NULL as dep_com,secteur,type_usage,type_bati,energie,conso,ges  from t_bat_3;


END
$$

