# Données BATISTATO

> Description des jeux de données utilisés dans l'application


### bat_a_ch_clefs.csv

Fichier CSV avec les chiffres clefs présentés dans la page d'accueil

-> lu au démarrage de l'application


### bat_dpe.rds

Données pour le DPE dans le portrait de territoire.

Script : `scripts/preparation-donnees.R`

Source : `inputs/bat_dpe.csv`


### bat_energie.rds

Données pour l'énergie dans le portrait de territoire.

Script : `scripts/preparation-donnees.R`

Source : `inputs/bat_energie.csv`


### bat_logement.rds

Données pour le logement dans le portrait de territoire.

Script : `scripts/preparation-donnees.R`

Source : `inputs/bat_logement.csv`


### bat_ter_assujetti.rds

Données pour le tertiaire assujetti dans le portrait de territoire.

Script : `scripts/preparation-donnees.R`

Source : `inputs/bat_ter_a.csv`


### bat_ter.rds

Données pour le tertiaire dans le portrait de territoire.

Script : `scripts/preparation-donnees.R`

Source : `inputs/bat_ter.csv`


### centroids_communes.rds

Centre des communes pour la carte avec les cercles.

Script : `scripts/preparation-centroids.R`

Source : tables geo (commune)


### centroids_departements.rds

Centre des départements pour la carte avec les cercles.

Script : `scripts/preparation-centroids.R`

Source : tables geo (departements)


### centroids_epci.rds

Centre des EPCI pour la carte avec les cercles.

Script : `scripts/preparation-centroids.R`

Source : tables geo (epci)


### chiffres_clefs.rds

Chiffres clefs affichés dans la page portrait pour un territoire.

Script : `scripts/preparation-chiffres-clefs.R`

Source : `inputs/bat_chiffres_clef.csv`


### choix_territoire.rds

Table utilisée pour la sélection d'un territoire.

Script : `scripts/preparation-table-choix-territoire.R`

Source : tables geo (communes, epci, departements)


### communes.rds

Contour géographique des communes.

Script : `scripts/preparation-tables-geo.R`

Source : `inputs/L_COMMUNE_ARRDT_EPCI_EPT_BDT_S_R11_2020.csv`


### departements.rds

Contour géographique des départements.

Script : `scripts/preparation-tables-geo.R`

Source : `inputs/L_DEP_BDT_S_R11.csv`


### epci.rds

Contour géographique des EPCI.

Script : `scripts/preparation-tables-geo.R`

Source : `inputs/L_EPCI_EPT_BDT_S_R11_2020.csv`


### indicateurs_metadonnees.rds

Métadonnées des indicateurs pour la page cartographie.

Script : `scripts/preparation-metadonnees.R`

Source : `inputs/indicateurs.ods`


### indicateurs.rds

Données utilisées pour les cartes.

Script : `scripts/preparation-indicateurs.R`

Source : `inputs/bat_chiffres_clef.csv`


### region.rds

Contour géographique de l'Ile-de-France

Script : `scripts/preparation-tables-geo.R`

Source : tables geo (departements)

