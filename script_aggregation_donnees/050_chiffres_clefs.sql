DO
$$
BEGIN


/* Le nom de la table finale table_finale_energie est à modifier dans tout le code */
/* Le nom de la table finale table_finale_tertiaire est à modifier dans tout le code */
/* Le nom de la table finale table_finale_logement est à modifier dans tout le code */
/* Le nom de la table finale table_finale_dpe est à modifier dans tout le code */
/* Le nom de la table finale table_finale_chiffres_clefs est à modifier dans tout le code */


	-- 1. CREER LA TABLE
	/*
	-- supprimer la table si elle existe
	drop table if exists table_finale_chiffres_clefs;

	-- créer la table 
	create table table_finale_chiffres_clefs (
		maille			varchar(8) NOT NULL,
		idcom 			varchar(5) ,
		siren_epci		varchar(254),
		dep_com			varchar(254),
		nrj_1 numeric,
		nrj_2 numeric,
		nrj_3 numeric,
		nrj_4 numeric,
		nrj_5 numeric,
		nrj_6 numeric,
		nrj_7 numeric,
		nrj_8 numeric,
		nrj_9 numeric,
		nrj_10 numeric,
		log_1 numeric,
		log_2 numeric,
		log_3 numeric,
		log_4 numeric,
		log_5 numeric,
		log_6 numeric,
		log_7 numeric,
		log_8 numeric,
		log_9 numeric,
		log_10 numeric,
		ter_1 numeric,
		ter_2 numeric,
		ter_3 numeric,
		ter_4 numeric,
		ter_5 numeric,
		ter_6 numeric,
		ter_7 numeric,
		ter_8 numeric,
		ter_9 numeric,
		ter_10 numeric,
		dpe_1 numeric,
		dpe_2 numeric,
		dpe_3 numeric,
		dpe_4 numeric,
		dpe_5 numeric,
		dpe_6 numeric,
		dpe_7 numeric,
		dpe_8 numeric,
		dpe_9 numeric,
		dpe_10 numeric);
	*/
    -- 2. FAIRE LES CALCULS
	-- Energie
	-- supprimer la table temporaire si elle existe
		drop table if exists t_bat_ch_clef_nrj;
		create temporary table t_bat_ch_clef_nrj
		as select maille,idcom,siren_epci,dep_com,
		sum(conso) as nrj_1, -- Consommation totale du territoire
		ROUND(sum(case when secteur='TER' or secteur='RES' then conso else 0 end)) as nrj_2, -- Consommation du secteur bâtiment
		ROUND(sum(case when secteur='TER' or secteur='RES' then conso else 0 end)/sum(conso)*100) as nrj_3, -- Consommation du secteur bâtiment par rapport à la consommation totale du territoire
		ROUND(sum(case when secteur='TER' or secteur='RES' then conso else 0 end)/21000*58296) as nrj_4, -- Equivalent en production photovoltaïque
		ROUND(sum(case when secteur='TER' or secteur='RES' then conso else 0 end)/500000/12,1) as nrj_5, -- Equivalent en production nucléaire annuelle
		sum(case when secteur='RES' and type_usage='CHF' then conso else 0 end) as nrj_6,
		ROUND(sum(case when secteur='RES' then conso else 0 end)/sum(conso)*100) as nrj_7, -- 		Consommation du secteur résidentiel par rapport à la consommation totale du territoire
		ROUND(sum(case when secteur='TER' then conso else 0 end)/sum(conso)*100) as nrj_8, -- Consommation du secteur tertiaire par rapport à la consommation totale du territoire	
		ROUND(sum(case when secteur='RES' and type_usage='CHF' and energie='PP' then conso else 0 end)/sum(case when secteur='RES' and type_usage='CHF' then conso else 0 end)*100) as nrj_9, -- Part du charbon/ produits pétroliers dans le chauffage résidentiel
		ROUND(sum(case when secteur in ('RES','TER') and type_usage='CHF' then conso else 0 end)/sum(case when secteur in ('RES','TER') and type_usage='CHF' then conso else 0 end)) as nrj_10 -- Part du chauffage/ Consommation du bâtiment
		from table_finale_energie
		group by maille,idcom,siren_epci,dep_com;
	

		-- Tertiaire
		-- supprimer la table temporaire si elle existe	
		drop table if exists t_bat_ch_clef_ter;
		create temporary table t_bat_ch_clef_ter
		as select maille,idcom,siren_epci,dep_com,
		sum(case when assujetti='O' then surface else 0 end) as ter_1, -- surfaces assujetties
		sum(case when assujetti='O' then nombre else 0 end) as ter_3, -- nombre de locaux assujettis
		sum(case when assujetti='O' then surface else 0 end)/sum(surface) as ter_4, -- Part de surfaces assujetties
		sum(case when assujetti='O' then nombre else 0 end)/sum(nombre) as ter_5 -- Part de locaux assujettis			  
		from table_finale_tertiaire
		group by maille,idcom,siren_epci,dep_com
		having sum(nombre) >11 -- secret statistique niveau 1. 
		;
		

		drop table if exists t_bat_ch_clef_log;
		create temporary table t_bat_ch_clef_log
		as select maille,idcom,siren_epci,dep_com,
		sum(case when type_occupant='Individuel privé' and age in ('Avant 1948','1975-1990','1949-1974') then nombre else 0 end) as nb_IP_90,
		sum(case when type_occupant='Collectif privé' and age in ('Avant 1948','1975-1990','1949-1974') then nombre else 0 end)  as nb_CP_90,
		sum(case when type_occupant='Parc social' and age in ('Avant 1948','1975-1990','1949-1974') then nombre else 0 end) as nb_HLM_90,
		sum(case when type_occupant='Individuel privé' then nombre else 0 end) as nb_IP,
		sum(case when type_occupant='Collectif privé' then nombre else 0 end) as nb_CP,
		sum(case when type_occupant='Parc social' then nombre else 0 end) as nb_HLM
		from table_finale_logement
		group by maille,idcom,siren_epci,dep_com;
		
		drop table if exists t_bat_ch_clef_log_1;
		create temporary table t_bat_ch_clef_log_1
		as select maille,idcom,siren_epci,dep_com,
		case 
			when nb_IP=0 then null 
			when nb_IP<11 or nb_IP_90<11 then null -- secrétisation
			else round(nb_IP_90/nb_IP*100) 
		end as log_1,
		case 
			when nb_CP=0 then null 
			when nb_CP<11 or nb_CP_90<11 then null -- secrétisation
			else round(nb_CP_90/nb_CP*100) 
		end as log_2,
		case when nb_HLM=0 then null else round(nb_HLM_90/nb_HLM*100) end as log_3,
		nb_IP_90+nb_CP_90+nb_HLM_90 as log_4,
		round((nb_IP_90+nb_CP_90+nb_HLM_90)/(nb_HLM+nb_IP+nb_CP)*100) as log_5		  
		from t_bat_ch_clef_log
		;
		
		drop table if exists t_bat_ch_clef_dpe;
		create temporary table t_bat_ch_clef_dpe
		as select maille,idcom,siren_epci,dep_com,
		round((nb_f+nb_g)/(nb_a+nb_b+nb_c+nb_d+nb_e+nb_f+nb_g)*100) as dpe_1,
		round((nb_e+nb_f+nb_g)/(nb_a+nb_b+nb_c+nb_d+nb_e+nb_f+nb_g)*100) as dpe_2
		from table_finale_dpe
		where type_occupant is NULL
		;
		
		drop table if exists t_bat_ch_clef_c;
		create temporary table t_bat_ch_clef_c
		as select 
		t_nrj.maille,t_nrj.idcom,t_nrj.siren_epci,t_nrj.dep_com,
		t_nrj.nrj_1,t_nrj.nrj_2,t_nrj.nrj_3,t_nrj.nrj_4,t_nrj.nrj_5,t_nrj.nrj_6,
		t_nrj.nrj_7,t_nrj.nrj_8,t_nrj.nrj_9,t_nrj.nrj_10,
		t_ter.ter_1,
		t_log.log_1,t_log.log_2,t_log.log_3,t_log.log_4,t_log.log_5,
		t_dpe.dpe_1,t_dpe.dpe_2
		from t_bat_ch_clef_nrj as t_nrj
			full join t_bat_ch_clef_ter as t_ter
				on t_nrj.maille=t_ter.maille 
					and coalesce(t_nrj.idcom,t_nrj.siren_epci,t_nrj.dep_com,'REGION')=coalesce(t_ter.idcom,t_ter.siren_epci,t_ter.dep_com,'REGION')
			full join t_bat_ch_clef_log_1 as t_log
				on t_nrj.maille=t_log.maille 
					and coalesce(t_nrj.idcom,t_nrj.siren_epci,t_nrj.dep_com,'REGION')=coalesce(t_log.idcom,t_log.siren_epci,t_log.dep_com,'REGION')
			full join t_bat_ch_clef_dpe as t_dpe
				on t_nrj.maille=t_dpe.maille
					and coalesce(t_nrj.idcom,t_nrj.siren_epci,t_nrj.dep_com,'REGION')=coalesce(t_dpe.idcom,t_dpe.siren_epci,t_dpe.dep_com,'REGION')
		;
	

		-- vider la table
		truncate table_finale_chiffres_clefs;

		-- Insérer les nouvelles lignes
		insert into table_finale_chiffres_clefs 
		SELECT 
		maille,idcom,siren_epci,dep_com,
		nrj_1,nrj_2,nrj_3,nrj_4,nrj_5,nrj_6, nrj_7,nrj_8,nrj_9,nrj_10 as nrj_10,
		log_1,log_2,log_3,log_4,log_5 as log_5,NULL as log_6,NULL as log_7,NULL as log_8,NULL as log_9,NULL as log_10,
		ter_1,NULL as ter_2,NULL as ter_3,NULL as ter_4,NULL as ter_5,NULL as ter_6,NULL as ter_7,NULL as ter_8,NULL as ter_9,NULL as ter_10,
		dpe_1,dpe_2 as dpe_2,NULL as dpe_3,NULL as dpe_4,NULL as dpe_5,NULL as dpe_6,NULL as dpe_7,NULL as dpe_8,NULL as dpe_9,NULL as dpe_10
		from t_bat_ch_clef_c;

		-- 3. EXPORTER
		/*

		select * from table_finale_chiffres_clefs;
		select * from table_finale_chiffres_clefs where idcom='78646';

		*/

END
$$

