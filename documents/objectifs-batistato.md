## Objectif de Batistato {.mt-0}


BATISTATO est un outil permettant de visualiser simplement la constitution des parcs de bâtiments résidentiels et tertiaires des territoires franciliens ainsi que leurs consommations énergétiques associées.

Il met à disposition des données agrégées, brutes ou sous forme de graphiques et permet des comparaisons entre territoires.

Cet outil a pour objectif de faciliter la prise de décision locale et de participer à l’accélération et à la massification de la rénovation énergétique à l’échelle francilienne.

*Qui : DRIEAT Ile-de-France*  
*Pour qui : Acteurs territoriaux franciliens*  
*Quand : mise à disposition de données mises à jour annuellement*  
*Où : Ile-de-France*  
*Quoi : outil de datavisualisation numérique statistique, service public*  
*Comment : description des parcs résidentiels et tertiaires et des consommations énergétiques associées*  
*Pourquoi : facilitation, accélération et massification de la rénovation énergétique en Ile-de-France*
