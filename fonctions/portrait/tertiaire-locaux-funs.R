
#  ------------------------------------------------------------------------
#
# Title : Batistato - Fonctions Module : Tertiaire - Locaux
#    By : DRIEAT
#  Date : 2023-01
#
#  ------------------------------------------------------------------------


afficher_graphique_n_logements_par_surface <- function(donnees) {
  donnees %>%
    arrange(categorie_surface) %>% 
    group_by(categorie_surface) %>% 
    summarise(
      nombre = sum(nombre, na.rm = TRUE)
    ) %>% 
    graphique_barres_horizontales(
      titre = "Répartition par palier de surface",
      sous_titre = "Source: base éco-énergie tertiaire, CEREMA, millésime 2021 / Traitements : DRIEAT",
      variable_x = "categorie_surface",
      variable_y = "nombre",
      surface = FALSE,
      label_y = "Nombre de locaux"
    ) %>% 
    ax_chart(
      toolbar = list(
        export = list(
          csv = list(filename = "repartition-palier-surface-nombre"),
          svg = list(filename = "repartition-palier-surface-nombre"),
          png = list(filename = "repartition-palier-surface-nombre")
        )
      )
    )
  # ax_plotOptions(bar = bar_opts(
  #   barHeight = 30
  # ))
}
#afficher_graphique_n_logements_par_surface(bat_ter_assujetti)


afficher_graphique_surface_par_surface <- function(donnees) {
  donnees  %>%
    arrange(categorie_surface) %>% 
    group_by(categorie_surface) %>% 
    summarise(
      surface = sum(surface, na.rm = TRUE)
    ) %>% 
    graphique_barres_horizontales(
      titre = "Répartition par palier de surface",
      sous_titre = "Source: base éco-énergie tertiaire, CEREMA, millésime 2021 / Traitements : DRIEAT",
      variable_x = "categorie_surface",
      variable_y = "surface",
      surface = TRUE,
      label_y = "Surface (m²)"
    ) %>% 
    ax_chart(
      toolbar = list(
        export = list(
          csv = list(filename = "repartition-palier-surface"),
          svg = list(filename = "repartition-palier-surface"),
          png = list(filename = "repartition-palier-surface")
        )
      )
    )
  # ax_plotOptions(bar = bar_opts(
  #   barHeight = 30
  # ))
}
# afficher_graphique_surface_par_surface(bat_ter_assujetti)



afficher_graphique_n_logements_par_assujetti <- function(donnees) {
  donnees %>% 
    select(assujetti, nombre, surface) %>% 
    group_by(assujetti) %>% 
    summarise(
      "Nombre de locaux" = sum(nombre, na.rm = TRUE),
      "Surface (m²)" = sum(surface, na.rm = TRUE)
    ) %>% 
    graphique_camembert(
      titre = "Nombre de locaux et surfaces assujettis (Nombre de locaux)",
      sous_titre = "Source: base éco-énergie tertiaire, CEREMA, millésime 2021 / Traitements : DRIEAT",
      variable_x = "assujetti",
      variable_y = "Nombre de locaux",
      surface = FALSE
    ) %>% 
    ax_legend(inverseOrder = TRUE) %>% 
    ax_tooltip(
      y = list(
        formatter = format_num(format = ",.2~f", locale = "fr-FR")
      )
    ) %>% 
    ax_chart(
      toolbar = list(
        export = list(
          csv = list(filename = "nombre-locaux-assujettis"),
          svg = list(filename = "nombre-locaux-assujettis"),
          png = list(filename = "nombre-locaux-assujettis")
        )
      )
    )
}
# afficher_graphique_n_logements_par_assujetti(bat_ter)


afficher_graphique_surface_par_assujetti <- function(donnees) {
  donnees %>% 
    select(assujetti, nombre, surface) %>% 
    group_by(assujetti) %>% 
    summarise(
      "Nombre de locaux" = sum(nombre, na.rm = TRUE),
      "Surface (m²)" = sum(surface, na.rm = TRUE)
    ) %>% 
    graphique_camembert(
      titre = "Nombre de locaux et surfaces assujettis (Surface (m²))",
      sous_titre = "Source: base éco-énergie tertiaire, CEREMA, millésime 2021 / Traitements : DRIEAT",
      variable_x = "assujetti",
      variable_y = "Surface (m²)",
      surface = TRUE
    ) %>% 
    ax_legend(inverseOrder = TRUE) %>% 
    ax_chart(
      toolbar = list(
        export = list(
          csv = list(filename = "surfaces-assujetties"),
          svg = list(filename = "surfaces-assujetties"),
          png = list(filename = "surfaces-assujetties")
        )
      )
    )
}
#afficher_graphique_surface_par_assujetti(bat_ter)



comptage_locaux_tertiaires_assujettis <- function(donnees) {
  donnees %>% 
    filter(assujetti == "Oui") %>% 
    group_by(assujetti) %>% 
    summarise(
      locaux_assujetti = sum(nombre, na.rm = TRUE),
      surface_assujetti = sum(surface, na.rm = TRUE)
    )
}    
# bat_ter %>% 
#   filter(code_territoire %in% c("78646")) %>%
#   comptage_locaux_tertiaires_assujettis() %>% 
#   pull("locaux_assujetti")




# Nombre de locaux et surface par usage (AVEC ASSUJETTI) -----------------

afficher_graphique_n_logements_par_usage_assujetti <- function(donnees) {
  donnees <- donnees %>%
    select(assujetti, categorie, nombre) %>%
    group_by(assujetti, categorie) %>%
    summarise(nombre = sum(nombre, na.rm = TRUE), .groups = "drop") %>%
    filter(!is.na(categorie)) %>% 
    mutate(
      tri_categorie = case_when(
        categorie %in% c("Autre") ~ 2,
        TRUE ~ 1
      )
    ) %>% 
    mutate(assujetti = factor(assujetti, levels = c("Oui", "Non"), ordered = TRUE)) %>% 
    arrange(assujetti, tri_categorie)
  
  graphique_barres_horizontales_empilees(
      donnees = donnees,
      titre = "Nombre de locaux et surfaces assujettis",
      sous_titre = "Source: base éco-énergie tertiaire, CEREMA, millésime 2021 / Traitements : DRIEAT",
      variable_x = "categorie",
      variable_y = "nombre",
      groupe = "assujetti",
      surface = FALSE,
      label_y = "Nombre de locaux assujettis par usage"
    ) %>%
    ax_colors_manual(couleurs_modalite(donnees$assujetti)) %>% 
    ax_tooltip(
      y = list(
        formatter = format_num(format = ",.2~f", locale = "fr-FR")
      )
    ) %>% 
    ax_chart(
      toolbar = list(
        export = list(
          csv = list(filename = "nombre-locaux-assujettis-usage"),
          svg = list(filename = "nombre-locaux-assujettis-usage"),
          png = list(filename = "nombre-locaux-assujettis-usage")
        )
      )
    )
}
# afficher_graphique_n_logements_par_usage_assujetti(bat_ter)


afficher_graphique_surface_par_usage_assujetti <- function(donnees) {
  donnees <- donnees %>%
    select(assujetti, categorie, surface) %>%
    group_by(assujetti, categorie) %>%
    summarise(surface = sum(surface, na.rm = TRUE), .groups = "drop") %>%
    filter(!is.na(categorie)) %>% 
    mutate(
      tri_categorie = case_when(
        categorie %in% c("Autre") ~ 2,
        TRUE ~ 1
      )
    ) %>% 
    mutate(assujetti = factor(assujetti, levels = c("Oui", "Non"), ordered = TRUE)) %>% 
    arrange(assujetti, tri_categorie)
  
  graphique_barres_horizontales_empilees(
    donnees = donnees,
    titre = "Nombre de locaux et surfaces assujettis",
    sous_titre = "Source: base éco-énergie tertiaire, CEREMA, millésime 2021 / Traitements : DRIEAT",
    variable_x = "categorie",
    variable_y = "surface",
    groupe = "assujetti",
    surface = TRUE,
    label_y = "Surface de tertiaire assujettie par usage"
  ) %>%
    ax_colors_manual(couleurs_modalite(sort(donnees$assujetti))) %>% 
    ax_chart(
      toolbar = list(
        export = list(
          csv = list(filename = "surfaces-assujetties-usage"),
          svg = list(filename = "surfaces-assujetties-usage"),
          png = list(filename = "surfaces-assujetties-usage")
        )
      )
    )
}
# afficher_graphique_surface_par_usage_assujetti(bat_ter)





# Graphique pour plusieurs territoires ------------------------------------

afficher_graphique_n_logements_par_surface_plusieurs <- function(donnees) {
  donnees %>%
    arrange(categorie_surface) %>% 
    group_by(categorie_surface, label) %>% 
    summarise(
      nombre = sum(nombre, na.rm = TRUE),
      .groups = "drop"
    ) %>% 
    complete(categorie_surface, label, fill = list("nombre" = 0)) %>%
    arrange(categorie_surface) %>% 
    graphique_barres_horizontales_empilees_100(
      titre = "Nombre de logements",
      sous_titre = NULL,
      variable_x = "label",
      variable_y = "nombre",
      groupe = "categorie_surface",
      surface = FALSE,
      suffix = NULL
    ) %>% 
    ax_tooltip(
      y = list(
        formatter = format_num(format = ",.2~f", locale = "fr-FR")
      )
    )
}
# bat_ter_assujetti %>%
#   filter(idcom %in% c(78383, 95555)) %>%
#   afficher_graphique_n_logements_par_surface_plusieurs()

afficher_graphique_surface_par_surface_plusieurs <- function(donnees) {
  donnees  %>%
    arrange(categorie_surface) %>% 
    group_by(categorie_surface, label) %>% 
    summarise(
      surface = sum(surface, na.rm = TRUE),
      .groups = "drop"
    ) %>% 
    complete(categorie_surface, label, fill = list("surface" = 0)) %>%
    arrange(categorie_surface) %>% 
    graphique_barres_horizontales_empilees_100(
      titre = "Surface (m²)",
      sous_titre = NULL,
      variable_x = "label",
      variable_y = "surface",
      groupe = "categorie_surface",
      surface = TRUE,
      suffix = NULL
    )
}
# bat_ter_assujetti %>%
#   filter(idcom %in% c(78383, 95555)) %>%
#   afficher_graphique_surface_par_surface_plusieurs()


afficher_graphique_n_logements_par_assujetti_plusieurs <- function(donnees) {
  donnees %>% 
    group_by(assujetti, label, tri_echelons, code_territoire) %>% 
    summarise(
      n_locaux = sum(nombre, na.rm = TRUE),
      # "Surface (m²)" = sum(surface, na.rm = TRUE),
      .groups = "drop"
    ) %>% 
    complete(assujetti, label) %>%
    mutate(assujetti = factor(assujetti, levels = c("Oui", "Non"), ordered = TRUE)) %>% 
    arrange(assujetti, tri_echelons) %>% 
    graphique_barres_horizontales_empilees_100(
      titre = "Nombre de locaux et surfaces assujettis",
      sous_titre = "Source: base éco-énergie tertiaire, CEREMA, millésime 2021 / Traitements : DRIEAT",
      variable_x = "label",
      variable_y = "n_locaux",
      groupe = "assujetti",
      surface = FALSE,
      suffix = NULL,
      label_y = "Nombre de locaux"
    ) %>% 
    ax_tooltip(
      y = list(
        formatter = format_num(format = ",.2~f", locale = "fr-FR")
      )
    ) %>% 
    # ax_legend(inverseOrder = TRUE) %>% 
    ax_chart(
      toolbar = list(
        export = list(
          csv = list(filename = "surfaces-assujetties"),
          svg = list(filename = "surfaces-assujetties"),
          png = list(filename = "surfaces-assujetties")
        )
      )
    )
}
# bat_ter %>%
#   filter(idcom %in% c(78383, 95555)) %>%
#   afficher_graphique_n_logements_par_assujetti_plusieurs()


afficher_graphique_surface_par_assujetti_plusieurs <- function(donnees) {
  donnees %>% 
    group_by(assujetti, label, tri_echelons, code_territoire) %>% 
    summarise(
      n_surface = sum(surface, na.rm = TRUE),
      .groups = "drop"
    ) %>% 
    complete(assujetti, label) %>% 
    mutate(assujetti = factor(assujetti, levels = c("Oui", "Non"), ordered = TRUE)) %>% 
    arrange(assujetti, tri_echelons) %>% 
    graphique_barres_horizontales_empilees_100(
      titre = "Nombre de locaux et surfaces assujettis",
      sous_titre = "Source: base éco-énergie tertiaire, CEREMA, millésime 2021 / Traitements : DRIEAT",
      variable_x = "label",
      variable_y = "n_surface",
      groupe = "assujetti",
      surface = TRUE,
      suffix = NULL,
      label_y = "Surface (m²)"
    ) %>% 
    # ax_legend(inverseOrder = TRUE) %>% 
    ax_chart(
      toolbar = list(
        export = list(
          csv = list(filename = "surfaces-assujetties"),
          svg = list(filename = "surfaces-assujetties"),
          png = list(filename = "surfaces-assujetties")
        )
      )
    )
}
# bat_ter %>%
#   filter(idcom %in% c(78383, 95555)) %>%
#   afficher_graphique_surface_par_assujetti_plusieurs(bat_ter)
