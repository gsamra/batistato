
## Aperçu géographique

```{r, eval=un_seul}
if (params$code_territoire == "200054781") {
carto_init() %>%
  setView(lng = 2.3522219, lat = 48.856614, zoom = 8) %>%
  addPolygons(
    data = departements,
    weight = 2,
    dashArray = "4",
    color = "#1D1C1D",
    opacity = 1,
    fill = TRUE,
    fillColor = "#6E6E6E",
    fillOpacity = 0
  ) 

} else if (params$code_territoire == "11") {
  carto_init() %>%
    setView(lng = 2.3522219, lat = 48.856614, zoom = 8) %>%
    addPolygons(
      data = departements,
      weight = 2,
      dashArray = "4",
      color = "#1D1C1D",
      opacity = 1,
      fill = TRUE,
      fillColor = "#6E6E6E",
      fillOpacity = 0
    ) %>% 
    addPolygons(
      data = region,
      weight = 4,
      color = "#FE0100",
      fill = TRUE,
      opacity = 1,
      fillColor = "#FFF",
      fillOpacity = 0
    )
} else {
ce_territoire <- bind_rows(
  departements %>%
    mutate(insee_dep = as.character(insee_dep)) %>%
    filter(insee_dep %in% params$code_territoire) %>%
    mutate(label = sprintf("%s - %s", insee_dep, tools::toTitleCase(tolower(nom_dep)))) %>%
    select(label),
  epci %>%
    mutate(id_ept_epci = as.character(id_ept_epci)) %>%
    filter(id_ept_epci %in% params$code_territoire) %>%
    mutate(label = nom_groupe) %>%
    select(label),
  communes %>%
    mutate(code_insee = as.character(code_insee)) %>%
    filter(code_insee %in% params$code_territoire) %>%
    mutate(label = sprintf("%s - %s", dep_com, tools::toTitleCase(tolower(nom_com)))) %>%
    select(label)
)

carto_init() %>%
  # setView(lng = 2.3522219, lat = 48.856614, zoom = 8) %>%
  addPolygons(
    data = departements,
    weight = 2,
    dashArray = "4",
    color = "#1D1C1D",
    opacity = 1,
    fill = TRUE,
    fillColor = "#6E6E6E",
    fillOpacity = 0
  ) %>%
  addPolygons(
    data = ce_territoire,
    weight = 4,
    color = "#FE0100",
    fill = TRUE,
    opacity = 1,
    fillColor = "#FFF",
    fillOpacity = 0,
    label = ~label
  ) %>% 
  ajuster_zoom_bbox(ce_territoire)
}
```


```{r, eval=plusieurs}

# Exception si comparaison QUE Métropole du Grand Paris et Région IDF
if (identical(params$code_territoire, c("11", "200054781")) | identical(params$code_territoire, c("200054781", "11"))) {
  carto_init() %>%
    setView(lng = 2.3522219, lat = 48.856614, zoom = 8) %>%
    addPolygons(
      data = departements,
      weight = 2,
      dashArray = "4",
      color = "#1D1C1D",
      opacity = 1,
      fill = TRUE,
      fillColor = "#6E6E6E",
      fillOpacity = 0
    ) %>% 
    addPolygons(
      data = region,
      weight = 4,
      color = "#FE0100",
      fill = TRUE,
      opacity = 1,
      fillColor = "#FFF",
      fillOpacity = 0
    )
} else {
  ce_territoire <- bind_rows(
  departements %>%
    mutate(insee_dep = as.character(insee_dep)) %>%
    filter(insee_dep %in% params$code_territoire) %>%
    mutate(label = sprintf("%s - %s", insee_dep, tools::toTitleCase(tolower(nom_dep)))) %>%
    select(label),
  epci %>%
    mutate(id_ept_epci = as.character(id_ept_epci)) %>%
    filter(id_ept_epci %in% params$code_territoire) %>%
    mutate(label = nom_groupe) %>%
    select(label),
  communes %>%
    mutate(code_insee = as.character(code_insee)) %>%
    filter(code_insee %in% params$code_territoire) %>%
    mutate(label = sprintf("%s - %s", dep_com, tools::toTitleCase(tolower(nom_com)))) %>%
    select(label)
  )
  
  carte <- carto_init() %>%
  setView(lng = 2.3522219, lat = 48.856614, zoom = 8) %>%
  addPolygons(
    data = departements,
    weight = 2,
    dashArray = "4",
    color = "#1D1C1D",
    opacity = 1,
    fill = TRUE,
    fillColor = "#6E6E6E",
    fillOpacity = 0
  ) %>%
  addPolygons(
    data = ce_territoire,
    weight = 4,
    color = "#FE0100",
    fill = TRUE,
    opacity = 1,
    fillColor = "#FFF",
    fillOpacity = 0,
    label = ~label
  )
  
  if ("11" %in% params$code_territoire) {
    carte %>% 
    addPolygons(
      data = region,
      weight = 4,
      color = "#FE0100",
      fill = TRUE,
      opacity = 1,
      fillColor = "#FFF",
      fillOpacity = 0
    )
    
  } else {
    carte
  }
}
  
  
# ce_territoire <- bind_rows(
#   departements %>%
#     mutate(insee_dep = as.character(insee_dep)) %>%
#     filter(insee_dep %in% params$code_territoire) %>%
#     mutate(label = sprintf("%s - %s", insee_dep, tools::toTitleCase(tolower(nom_dep)))) %>%
#     select(label),
#   epci %>%
#     mutate(id_ept_epci = as.character(id_ept_epci)) %>%
#     filter(id_ept_epci %in% params$code_territoire) %>%
#     mutate(label = nom_groupe) %>%
#     select(label),
#   communes %>%
#     mutate(code_insee = as.character(code_insee)) %>%
#     filter(code_insee %in% params$code_territoire) %>%
#     mutate(label = sprintf("%s - %s", dep_com, tools::toTitleCase(tolower(nom_com)))) %>%
#     select(label)
# )
# 
# 
# carto_init() %>%
#   setView(lng = 2.3522219, lat = 48.856614, zoom = 8) %>%
#   addPolygons(
#     data = departements,
#     weight = 2,
#     dashArray = "4",
#     color = "#1D1C1D",
#     opacity = 1,
#     fill = TRUE,
#     fillColor = "#6E6E6E",
#     fillOpacity = 0
#   ) %>%
#   addPolygons(
#     data = ce_territoire,
#     weight = 4,
#     color = "#FE0100",
#     fill = TRUE,
#     opacity = 1,
#     fillColor = "#FFF",
#     fillOpacity = 0,
#     label = ~label
#   )
```

