DO
$$
BEGIN


/* Le nom de la table finale table_communes_insee est à modifier dans tout le code */
/* Le nom de la table des locaux fichiers fonciers table_locaux_ff est à remplacer dans tout le code */ 
/* Le nom de la table rpls table_rpls est à remplacer dans tout le code */ 
/* Le nom de la table finale table_finale_logement est à modifier dans tout le code */


/*
	--1. CREER LES TABLES
	-- supprimer la table si elle existe
	drop table if exists table_finale_logement;

	-- créer la table 
	create table table_finale_logement (
		maille character varying(8) NOT NULL,
		idcom 			varchar(5) ,
		siren_epci		varchar(254),
		dep_com			varchar(254),
		age				text,
		type_occupant text,
		type_occupant_d text,
		nombre			numeric,
		surface			numeric,
		secret_f		char(1)
	);
	
*/
	-- 2. CALCULS A LA COMMUNE
	-- supprimer la table temporaire si elle existe
	-- Communes nécessaires
	drop table if exists t_communes;
	create temporary table t_communes
	as select code_insee AS idcom
	from table_communes_insee
	where dep_com <>'28'
	;
	
	-- Sélectionner les colonnes nécessaires
	drop table if exists t_batistato_logement_0;
	create temporary table t_batistato_logement_0 
	as select c.idcom,
	dteloc,loghlls,catpro3,ccthp,jannath,stoth,hlmsem,loghvac,proba_rprs
	from t_communes c
	left join table_locaux_ff l
	on c.idcom=l.idcom
	where l.logh='t'-- logh='t'=logement
	;
	
	-- Créer les nouvelles colonnes
	drop table if exists t_batistato_logement_1;
	create temporary table t_batistato_logement_1 
	as 
	select idcom,
	case when jannath <= 1948 then 'Avant 1948'
		when jannath <= 1974 then '1949-1974'
		when jannath <= 1990 then '1975-1990'
		when jannath <= 2005 then '1991-2005'
		when jannath > 2005 then 'Après 2005'
		else 'Inconnu' end as age, -- catégorie d'années de construction
	case when dteloc='1' then 'I'
	when dteloc = '2' then 'C'
	else NULL end as Individuel_collectif, -- Individuel ou collectif
	case when hlmsem in ('5','6') then 'O'
	else 'N' end as HLM,
	case when ccthp='P' then 'O' else 'N' end as proprietaire,
	stoth,1 as nombre
	from t_batistato_logement_0 t;
	
	-- Grouper
	drop table if exists t_batistato_logement_2;
	create temporary table t_batistato_logement_2 
	as select idcom,age,individuel_collectif,proprietaire,sum(stoth) as surface,count(nombre) as nombre
	from t_batistato_logement_1
	where HLM='N' -- Enlever les HLM
	group by idcom,age,individuel_collectif,proprietaire;
	
	-- Type d'occupant
	drop table if exists t_batistato_logement_3;
	create temporary table t_batistato_logement_3 
	as select idcom,age,
	case when  individuel_collectif='I'  then 'Individuel privé'
		 when individuel_collectif='C' then 'Collectif privé'
		 else NULL end as type_occupant,
	case when individuel_collectif='I' and proprietaire='O' then 'Individuel privé - Propriétaire occupant'
		 when individuel_collectif='C' and proprietaire='O' then 'Collectif privé - Propriétaire occupant'
		 when individuel_collectif='I' and proprietaire='N' then 'Individuel privé - Locatif'
		 when individuel_collectif='C' and proprietaire='N' then 'Collectif privé - Locatif'
		 else NULL end as type_occupant_d,
	sum(surface) as surface,sum(nombre) as nombre
	from t_batistato_logement_2
	group by idcom,age,type_occupant,type_occupant_d
	;
	
	-- CALCULS HLM
	
	-- Sélectionner les colonnes nécessaires
	drop table if exists t_batistato_hlm_0;
	create temporary table t_batistato_hlm_0 
	as select depcom,typeconst,
	construct,surfhab
	from table_rpls l
	;
	
	-- Créer les nouvelles colonnes
	drop table if exists t_batistato_hlm_1;
	create temporary table t_batistato_hlm_1 
	as 
	select depcom as idcom,
	case when typeconst='I' then 'Parc social - Individuel' else 'Parc social - Collectif' end as type_occupant_d,
	case when construct <= 1948 then 'Avant 1948'
		when construct <= 1974 then '1949-1974'
		when construct <= 1990 then '1975-1990'
		when construct <= 2005 then '1991-2005'
		when construct > 2005 then 'Après 2005'
		else 'Inconnu' end as age, -- catégorie d'années de construction,
	surfhab,1 as nombre
	from t_batistato_hlm_0 t
	;
	
	-- Type d'occupant
	drop table if exists t_batistato_hlm_2;
	create temporary table t_batistato_hlm_2 
	as select idcom,age,type_occupant_d,
	sum(surfhab) as surface,sum(nombre) as nombre
	from t_batistato_hlm_1
	group by idcom,age,type_occupant_d
	;

	truncate table_finale_logement;
	insert into table_finale_logement
	select 'COMMUNE' as maille,idcom,NULL as siren_epci,null as dep_com,
	age,type_occupant,type_occupant_d,nombre,surface
	from t_batistato_logement_3;
	
	insert into table_finale_logement
	select 'COMMUNE' as maille,idcom,NULL as siren_epci,null as dep_com,
	age,'Parc social' as type_occupant,type_occupant_d,nombre,surface
	from t_batistato_hlm_2;

	-- 3. GROUPEMENT EPCI, DEPARTEMENT, REGION
	DELETE
	FROM table_finale_logement
	WHERE maille<>'COMMUNE';
	
	
	/*Communes et leurs EPCI hors département 28*/
	drop table if exists t_communes;
	create temporary table t_communes
	as select code_insee,siren_epci  -- EPCI
	from table_communes_insee
	where dep_com<>'28' and siren_epci is not null
	union
	select code_insee,id_ept as siren_epci -- EPT
	from table_communes_insee
	where dep_com<>'28' and id_ept is not null
	; -- CC du pays Houdanais, Région Ile de France et département 28
	
	drop table if exists t_dep;
	create temporary table t_dep
	as select code_insee,dep_com 
	from table_communes_insee
	where dep_com<>'28' 
	;

	drop table if exists t_bat_1;
	create temporary table t_bat_1
	as select 'EPCI' as maille, c.siren_epci as siren_epci,
	age,type_occupant,type_occupant_d,sum(nombre) as nombre,sum(surface) as surface
	from table_finale_logement b_0 
		left join t_communes c
			on b_0.idcom=c.code_insee
	group by maille,c.siren_epci,age,type_occupant,type_occupant_d;
 
 	drop table if exists t_bat_2;
	create temporary table t_bat_2
	as select 'DEP' as maille, c.dep_com as dep_com,
	age,type_occupant,type_occupant_d,sum(nombre) as nombre,sum(surface) as surface	
	from table_finale_logement b_0 
		inner join t_dep c
			on b_0.idcom=c.code_insee 
	group by maille,c.dep_com,age,type_occupant,type_occupant_d;
 
  	drop table if exists t_bat_3;
	create temporary table t_bat_3
	as select 'REGION' as maille, 
	age,type_occupant,type_occupant_d,sum(nombre) as nombre,sum(surface) as surface
	from table_finale_logement b_t 
	group by maille,age,type_occupant,type_occupant_d;
	
	insert into table_finale_logement
	select maille,NULL as idcom,siren_epci,NULL as dep_com,age,type_occupant,type_occupant_d,nombre,surface 
	from t_bat_1;

	insert into table_finale_logement
	select maille,NULL as idcom,NULL as siren_epci,dep_com,age,type_occupant,type_occupant_d,nombre,surface
	from t_bat_2;

	insert into table_finale_logement
	select maille,NULL as idcom,NULL as siren_epci,NULL as dep_com,age,type_occupant,type_occupant_d,nombre,surface
	from t_bat_3;
	
 	-- 4. SECRETISATION
	/*Tables de groupement communes et EPCI/EPT*/
		/*Communes et leurs EPCI hors département 28*/
		drop table if exists t_communes;
		create temporary table t_communes
		as select code_insee,siren_epci  -- EPCI
		from table_communes_insee
		where dep_com<>'28' and siren_epci is not null
		union
		select code_insee,id_ept as siren_epci -- EPT
		from table_communes_insee
		where dep_com<>'28' and id_ept is not null
		; -- Région Ile de France hors département 28

		drop table if exists t_dep;
		create temporary table t_dep
		as select code_insee,dep_com 
		from table_communes_insee
		where dep_com<>'28' 
		;
	/*table_finale_logement*/
		/*Secret statistique si nombre concerné<11*/
		UPDATE table_finale_logement
		SET secret_f = NULL;

		/*Secrétisation niveau 2 pour les EPCI/EPT*/	
		drop table if exists t_bat_2_s;
		create temporary table t_bat_2_s
		as
			select b_0.idcom, c.siren_epci,
			age,type_occupant_d,
			nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_finale_logement b_0 
				inner join t_communes c
					on b_0.idcom=c.code_insee
			where type_occupant <>'Parc social' -- pas besoin de secrétisation pour le parc social
		order by siren_epci,age,type_occupant_d,nombre asc
		;
		
		
		drop table if exists t_bat_2_s1;
		create temporary table t_bat_2_s1
		as
		select siren_epci,idcom,age,type_occupant_d,nombre,secret_f_num,
		ROW_NUMBER() over(partition by siren_epci,age,type_occupant_d order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by siren_epci,age) as secret_f_sum
		from t_bat_2_s
		order by siren_epci,age,type_occupant_d,nombre;
		
		/*Exemple :
		
		select * from t_bat_2_s1 where secret_f_sum=1 and secret_f_rank=2 and idcom='75101'
		*/
		
		/*Secrétisation niveau 2 pour les départements*/	
		drop table if exists t_bat_2_s2;
		create temporary table t_bat_2_s2
		as
			select b_0.idcom, c.dep_com,
			age,type_occupant_d ,nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_finale_logement b_0 
				inner join t_dep c
					on b_0.idcom=c.code_insee
			where type_occupant<>'Parc social'
		order by dep_com,age,type_occupant_d ,nombre asc
		;

		drop table if exists t_bat_2_s3;
		create temporary table t_bat_2_s3
		as
		select dep_com,idcom,age,type_occupant_d ,nombre,secret_f_num,
		ROW_NUMBER() over(partition by dep_com,age,type_occupant_d  order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by dep_com,age) as secret_f_sum
		from t_bat_2_s2;

		/*Exemple :
		
		select * from t_bat_2_s3 where secret_f_sum=1 and secret_f_rank=2 and idcom='75101'
		*/

		/*Somme à la commune et EPCI*/
		drop table if exists t_bat_2_s4;
		create temporary table t_bat_2_s4
		as
			select idcom,siren_epci,
			age,type_occupant,type_occupant_d,
			nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_finale_logement b_0 
			where type_occupant <>'Parc social'
		;	
		
		
		/*Secrétisation pour l'axe type_occupant_d*/
		drop table if exists t_bat_2_s5;
		create temporary table t_bat_2_s5
		as
			select b0.idcom,b0.siren_epci,b0.age,b0.type_occupant_d
			from t_bat_2_s4 b_secret
				left join table_finale_logement b0
					on coalesce(b_secret.idcom,b_secret.siren_epci)=coalesce(b0.idcom,b0.siren_epci)
						and b_secret.age=b0.Age
						and b_secret.type_occupant=b0.type_occupant
						and b_secret.type_occupant_d<>b0.type_occupant_d -- Somme type_occupant
			where b_secret.secret_f_num=1
		;	
		
		/*Secrétisation pour l'axe age > 1990*/
		drop table if exists t_bat_2_s6;
		create temporary table t_bat_2_s6
		as
			select b0.idcom,b0.siren_epci,b0.age,b0.type_occupant_d
			from t_bat_2_s4 b_secret
				left join table_finale_logement b0
					on coalesce(b_secret.idcom,b_secret.siren_epci)=coalesce(b0.idcom,b0.siren_epci)
						and b_secret.type_occupant_d=b0.type_occupant_d
						and (b_secret.age='1991-2005' and b0.age='Après 2005'
								or b_secret.age='Après 2005' and b0.age='1991-2005')
			where b_secret.secret_f_num=1
		;	
		
		/*Secrétisation age*/
		drop table if exists t_bat_2_s4_1;
		create temporary table t_bat_2_s4_1
		as
			select idcom,siren_epci,age,type_occupant_d,
			ROW_NUMBER() over(partition by coalesce(idcom,siren_epci),age order by nombre) as secret_f_rank,
			sum(secret_f_num) over(partition by coalesce(idcom,siren_epci),age) as secret_f_sum
			from t_bat_2_s4 
		;
		
		/*Secrétisation age*/
		drop table if exists t_bat_2_s7;
		create temporary table t_bat_2_s7
		as
			select idcom,siren_epci,type_occupant_d,age,
			ROW_NUMBER() over(partition by coalesce(idcom,siren_epci),type_occupant_d,age order by nombre) as secret_f_rank,
			sum(secret_f_num) over(partition by coalesce(idcom,siren_epci),type_occupant_d) as secret_f_sum
			from t_bat_2_s4 
			where age in ('Avant 1948','1949-1974','1975-1990')
		;
	
	
		/*Exemple:
		select * from t_bat_2_s7 where siren_epci='200017846'
		*/

		/*Union de tous les types de secrétisation*/
		drop table if exists t_bat_2_s8;
		create temporary table t_bat_2_s8
		as
		SELECT idcom,type_occupant_d,age
		FROM t_bat_2_s1
		WHERE secret_f_sum=1 -- secret niveau 2 : Quand il n'y a qu'un seul élément, il faut secrétiser une deuxième commune
		and secret_f_rank = 2
		union
		SELECT idcom,type_occupant_d,age
		FROM t_bat_2_s3
		WHERE secret_f_sum=1 -- secret niveau 2 : Quand il n'y a qu'un seul élément, il faut secrétiser une deuxième commune
		and secret_f_rank = 2
		union
		SELECT idcom,type_occupant_d,age
		from t_bat_2_s4_1
		WHERE secret_f_sum=1 -- secret niveau 2 : Quand il n'y a qu'un seul élément, il faut secrétiser une deuxième commune
		and secret_f_rank = 2
		AND idcom is not null
		union
		SELECT idcom,type_occupant_d,age
		FROM t_bat_2_s5
		WHERE idcom is not null
		union
		SELECT idcom,type_occupant_d,age
		FROM t_bat_2_s6
		WHERE idcom is not null
		union
		SELECT idcom,type_occupant_d,age
		FROM t_bat_2_s7
		WHERE secret_f_sum=1 -- secret niveau 2 : Age
		and secret_f_rank = 2		
		and idcom is not null
		;

		update table_finale_logement b
		set secret_f='2' 
		from t_bat_2_s8
		where b.idcom=t_bat_2_s8.idcom
								and b.age=t_bat_2_s8.age
								and b.type_occupant_d=t_bat_2_s8.type_occupant_d
								;
								
		drop table if exists t_bat_2_s9;
		create temporary table t_bat_2_s9
		as
		SELECT siren_epci,type_occupant_d,age
		from t_bat_2_s4_1
		WHERE secret_f_sum=1 -- secret niveau 2 : Quand il n'y a qu'un seul élément, il faut secrétiser une deuxième commune
		and secret_f_rank = 2
		AND idcom is not null
		union
		SELECT siren_epci,type_occupant_d,age
		FROM t_bat_2_s5
		WHERE siren_epci is not null
		union
		SELECT siren_epci,type_occupant_d,age
		FROM t_bat_2_s6
		WHERE siren_epci is not null
		union
		SELECT siren_epci,type_occupant_d,age
		FROM t_bat_2_s7
		WHERE secret_f_sum=1 -- secret niveau 2 : Age
		and secret_f_rank = 2		
		and siren_epci is not null
		;
			
		update table_finale_logement b
		set secret_f='2' 
		from t_bat_2_s9
		where b.siren_epci=t_bat_2_s9.siren_epci
								and b.age=t_bat_2_s9.age
								and b.type_occupant_d=t_bat_2_s9.type_occupant_d
								;
								
		UPDATE table_finale_logement
		SET secret_f='1'
		WHERE  	nombre<11
		and type_occupant<>'Parc social'
		;	-- secret de niveau 1, si le nombre de logements concernés est inférieur à 11 

		
/*
		-- 5. EXPORT
		select maille,idcom,siren_epci,dep_com,type_occupant,type_occupant_d,age,
		case when secret_f in ('1','2') then NULL else nombre end as nombre,
		case when secret_f in ('1','2') then NULL else surface end as surface
		from table_finale_logement

*/


END
$$

/*
SELECT * FROM t_b_ter_0 where idcom='78517' and categorie='Enseignement-recherche'
*/
