---
title: "Contexte"
---

## Les enjeux en Ile-de-France

**En Île-de-France, le secteur du bâtiment est le poste le plus consommateur d’énergie et le plus émetteur de gaz à effet de serre (GES).** D’après les données du ROSE (Inventaire 2019 – source Airparif 2022), il représente :

 * 68% des consommations d’énergie finale (39 % pour le résidentiel, 29 % pour le tertiaire) ;
 * 48% des émissions de GES régionales (30 % pour le résidentiel, 18 % pour le tertiaire).


**La rénovation énergétique des bâtiments s’impose donc comme une priorité :**

 * **écologique** : pour lutter contre le changement climatique, en agissant sur un levier majeur de réduction des émissions de GES ;
 * **énergétique** : pour réduire les consommations énergétiques et notre dépendance aux énergies fossiles ;
 * **sociale** : pour lutter contre la précarité énergétique et améliorer le confort ;
 * **économique** : pour dynamiser la filière et créer des emplois non-délocalisables ;


Les multiples bénéfices liés à la rénovation énergétique sont à envisager comme une réelle opportunité de transformation. Pour faire face à ce grand défi qu’est la transition écologique du secteur du bâtiment, **différents leviers** s’offrent à nous, nécessitant plus-ou-moins de moyens, avec des temps de retours sur investissement variables :

 * la **sobriété**, qui correspond à une tempérance dans les usages énergétiques ;
 * l’**efficacité**, qui consiste en la réduction de la quantité d’énergie nécessaire pour satisfaire un même besoin ;
 * la **décarbonation**, qui se définit par la réduction de l’utilisation d’énergie carbonée.


![](images/cadrage-1.png)


Ces efforts nécessaires pour atténuer l’impact du secteur du bâtiment sur le changement climatique sont également l’occasion d’adapter ces mêmes bâtiments aux conséquences d’un changement climatique inéluctable et déjà visible, et permettre de conserver un cadre bâti et urbain à la fois confortable et résilient face aux crises.



## Les engagements de la France à l’échelle internationale et européenne

### Les éléments de contexte à l’échelle internationale


 * Dès 1992, lors du Sommet de la Terre de Rio, se crée la Convention-cadre des Nations unies sur les changements climatiques (CCNUCC) qui fournit un cadre de coopération inter-gouvernemental sur la question des changements climatiques.
 
 * En 1997, le protocole de Kyoto engage les pays à stabiliser les émissions de gaz à effet de serre (GES). Les pays signataires dits « de l’annexe » (les pays développés ou en transition vers une économie de marché comme la Russie) acceptent globalement de réduire de -5,5% leurs émissions de gaz à effet de serre sur la période 2008-2012 par rapport au niveau atteint en 1990.
 
 * Suite à la COP21, le 12 décembre 2015, les Parties à la CCNUCC parviennent à l’Accord de Paris.
 
 
Objectifs :

 * limiter l’augmentation de la température mondiale à un niveau bien inférieur à 2° (atténuation des effets du changement climatique) ;
 * mettre en place des mesures pour faire face aux impacts du changement climatique (adaptation et résilience aux effets du changement climatique) ;
 * encourager la "finance verte", les innovations et le renforcement des capacités.
 
Toutes les Parties ont présenté des contributions déterminées au niveau national (NDCs) fixant leurs efforts pour atteindre les objectifs globaux de réduction des émissions de GES : les Parties doivent rendre compte régulièrement de la mise en œuvre des NDCs afin de permettre la réalisation d’un bilan mondial des émissions tous les 5 ans.


### Les éléments de contexte à l’échelle européenne

L’Union de l’Énergie (2015) représente l’instrument plus puissant de l’Union Européenne pour atteindre les objectifs communs de transition vers une économie zéro-carbone compétitive et innovante, créatrice d’emplois et de valeur ajoutée. Le règlement sur la gouvernance de l’Union de l’Énergie et de l’Action pour le climat (entré en vigueur le 24 décembre 2018) définit le cadre législatif commun pour les politiques en matière d’énergie et de climat dans les États membres de l’Union Européenne (UE).

Faisant suite au Paquet sur le climat et l’énergie, le paquet des mesures "Une énergie propre pour tous les Européens", entré en vigueur en 2019, traduit concrètement la stratégie de longue-terme de l’UE pour atteindre les objectifs de transition énergétique à l’horizon 2050. Les Pays Membres ont de 1 à 2 ans pour transporter cette directive dans leur propre législation. Le paquet se compose de 8 actes législatifs, dont la directive sur la performance énergétique des bâtiments, qui est entrée en vigueur dans le cadre législatif français avec la publication de l’Ordonnance n° 2020-866 du 15 juillet 2020.

À travers les Plans nationaux énergie-climat (PNEC), chaque État membre indique comment il contribuera à la réalisation des objectifs communs. Les PNEC ont le pouvoir de transformer l’Accord de Paris en actions concrètes, en permettant la mobilisation de moyens financiers et en encouragent la participation des citoyens et de la société civile à la transition écologique.

Les plans définitifs sont disponibles sur le site de la commission européenne, et concernent la période 2021-2030 :
[Le plan national énergie et climat de la France](https://www.ecologie.gouv.fr/https://energy.ec.europa.eu/system/files/2020-04/fr_final_necp_main_fr_0.pdf){target="_blank"}



## Les engagements nationaux

La France s’inscrit activement dans les dynamiques internationale et européenne et définit depuis quelques années des politiques de développement durable destinées à opérer une véritable transition énergétique, notamment à travers la rénovation énergétique du bâtiment.

### Stratégie Nationale Bas-Carbone (SNBC)

Introduite par la loi **n°2015-992 du 17 août 2015** de Transition Energétique pour la Croissance Verte (LTECV), la Stratégie Nationale Bas Carbone (SNBC), publiée en décembre 2015, est la feuille de route pour la mise en œuvre des politiques de lutte contre le changement climatique. Elle fixe la trajectoire pour réduire les émissions de GES et atteindre la neutralité carbone en 2050.

La SNBC a été adoptée pour la première fois en 2015, puis révisée en 2018-2019. Elle vise à atteindre **la neutralité carbone en 2050** (ambition rehaussée par rapport à la première SNBC qui visait le **facteur 4**, soit une réduction de 75 % de ses émissions GES à l'horizon 2050 par rapport à 1990). Ce projet de SNBC révisée a fait l’objet d’une consultation du public du 20 janvier au 19 février 2020. La nouvelle version de la SNBC et les budgets carbone pour les périodes 2019-2023, 2024-2028 et 2029-2033 ont été adoptés par décret le 21 avril 2020.

Selon la SNBC, en 2050, 70 % du parc bâti pourrait être constitué d’immeubles construits avant 2012. En conséquence, pour le secteur du bâtiment, le premier enjeu est la **rénovation thermique radicale** du parc existant, pour aboutir en moyenne au niveau assimilable aux normes bâtiment basse consommation (BBC) sur la totalité de ce parc en 2050, avec des exigences thermiques et énergétiques ambitieuses ainsi que des exigences fortes en matière d’émissions de gaz à effet de serre.

![](images/cadrage-2.png){width=40%}

*Figure 1: [Stratégie Nationale Bas-Carbone (SNBC)](https://www.ecologie.gouv.fr/strategie-nationale-bas-carbone-snbc){target="_blank"},  La Stratégie Nationale Bas-Carbone résumée en 4 pages*

### Le Plan de Rénovation Énergétique des Bâtiments (PREB)

Lancé le 24 novembre 2017, le PREB répond aux objectifs annoncés de la LTECV et fait de la rénovation énergétique des bâtiments une priorité nationale en France.

Le PREB s’articule en 12 actions organisées autour de 4 grands axes :

 * faire de la rénovation énergétique une priorité nationale identifiée, aux objectifs hiérarchisés ;
 * lutter contre la précarité énergétique et massifier la rénovation pour les particuliers ;
 * accélérer la rénovation et les économies d’énergie des bâtiments tertiaires, en particulier du parc public ;
 * favoriser la montée en compétence et les innovations de la filière de la rénovation énergétique du bâti.


#### Loi Energie-Climat (LEC)

La loi énergie et climat du 8 novembre 2019 vise à répondre à l’urgence écologique et climatique. Elle inscrit cette urgence dans le code de l’énergie ainsi que l’objectif d’une neutralité carbone en 2050, en divisant les émissions de gaz à effet de serre par six au moins d’ici cette date.


#### Le dispositif « éco-énergie tertiaire »

![](images/cadrage-3.png){width=50%}

*Figure 2: [Éco Énergie Tertiaire : construisons ensemble la transition énergétique](https://www.ecologie.gouv.fr/eco-energie-tertiaire-eet){target="_blank"}*

Initialement portée par la loi Grenelle 2 (2010), puis par la LTECV (2015), l’obligation pour les bâtiments, parties de bâtiments ou ensembles de bâtiments à usage tertiaire de réduction de la consommation d’énergie finale s’inscrit dorénavant dans le cadre de la loi sur l’Évolution du Logement et Aménagement Numérique (ELAN) du 24 novembre 2018 (art. 175). Le décret d’application de l’article 175 de la loi ELAN, dit « décret tertiaire », est entré en vigueur le 1er octobre 2019 et définit les modalités de mise en œuvre de l’obligation.

![](images/cadrage-4.png){width=50%}

*Figure 3: [Éco Énergie Tertiaire : construisons ensemble la transition énergétique](https://www.ecologie.gouv.fr/eco-energie-tertiaire-eet){target="_blank"}, La notation éco-énergie tertiaire*


Deux critères sont nécessaires pour qu’un local d’activité soit assujetti :

 * Critère typologique : l’activité hébergée doit relever du secteur tertiaire, qu’il soit marchand (bureaux, commerces, hôtellerie-restauration...) ou  non-marchand (services publics, enseignement, santé…) ;
 * Critère surfacique : la surface cumulée des activités tertiaires hébergées doit dépasser 1 000 m² à l’échelle du bâtiment ou de l’unité foncière.

Rappel : tous les bâtiments sont concernés, existants comme neufs.


Si ces deux critères sont respectés, les locaux concernés sont soumis à l’obligation de réduire progressivement leur consommation d’énergie finale selon deux modalités au choix :

 * valeur relative : −40 % en 2030, −50 % en 2040, −60 % en 2050, par rapport à une année de référence comprise entre 2010 et 2019). Cette modalité est à privilégier pour les bâtiments les moins performants.
 * valeur absolue : seuils fixés par catégories d’activité chaque décennie, en fonction des meilleurs techniques constructives disponibles. Cette modalité est appropriée aux bâtiments les plus récents et performants.

Les assujettis au dispositif ont également pour obligation de déclarer annuellement (30 septembre de chaque année) les données bâtimentaires et de consommations associées sur la plateforme numérique dédiée : **OPERAT**.


#### Loi "Climat et Résilience" du 22 août 2021 : Interdiction à la location des passoires thermiques

![](images/cadrage-5.png)


L’un des objectifs de la loi Climat et Résilience est de massifier la rénovation énergétique des logements. Pour ce faire, un certain nombre de mesures restrictives ont été adoptées, issues des conclusions de la Convention citoyenne pour le climat :

 * l’interdiction de la mise en location des logements dont la consommation en énergie finale est supérieure à 450 kW/m²/an à partir du 1er janvier 2023 ;
 * l’interdiction de la location de la location des logements de classe énergétique inférieure à F à partir du 1er janvier 2025 ;
 * l’interdiction de la location de la location des logements de classe énergétique inférieure à E à partir du 1er janvier 2028 ;
 * l’interdiction de la location de la location des logements de classe énergétique inférieure à D à partir du 1er janvier 2034 ;
 * le gel des loyers des passoires énergétiques (étiquettes F et G).


## Les engagements régionaux

### Le Schéma Régional du Climat de l’Air et de l’Energie (SRCAE, en cours de révision)

Le SRCAE, élaboré par l’État et le Conseil régional et approuvé le 14 décembre 2012, fixe la stratégie régionale de déclinaison des objectifs de transition énergétique.

Le SRCAE fixe trois grandes priorités régionales, déclinées en 17 objectifs et 58 orientations stratégiques :

 * le renforcement de l’efficacité énergétique des bâtiments avec un objectif de doublement du rythme des réhabilitations dans le tertiaire et de triplement dans le résidentiel ;
 * le développement du chauffage urbain alimenté par des énergies renouvelables et de récupération (EnR&R), avec un objectif d’augmentation de 40 % du nombre d’équivalent logements raccordés à un réseau d’ici 2020 ;
 * la réduction de 20 % des émissions de gaz à effet de serre du trafic routier.


Première des priorités du SRCAE, la rénovation énergétique des bâtiments s’articule autour des principaux objectifs (2050) :
 * de réduction de moitié des consommations énergétiques des bâtiments ;
 * de rénovation de 180 000 logements par an ;
 * de rénovation de 8 millions de m² de tertiaire par an ;
 * de rénovation de 100% du parc au niveau "BBC Rénovation".


### Logement : Schémas Régional de l’Habitat et de l’Hebergement

En Île-de-France, le Schéma Régional de l’Habitat et de l’Hébergement (SRHH) constitue le document stratégique régional de référence en matière d’habitat et d’hébergement. Élaboré par le Comité Régional de l’Habitat et de l’Hébergement (CRHH), il est porteur pour six années d’une vision partagée des enjeux et des priorités du territoire francilien. En matière de rénovation énergétique des logements, le document fixe des objectifs globaux par EPCI, c’est-à-dire le rythme de rénovation énergétique à tenir pour les trois segments du parc résidentiel que sont les maisons individuelles, le collectif et les logements sociaux.

##  Feuille de route pour la transition énergétique en Ile-de-France
Pour accélérer la transition énergétique en accompagnant au mieux les territoires, les acteurs en charge des politiques de transition énergétique pour l’État (Préfecture de région, DRIEAT, DRIHL, DRIAAF, ADEME, DDT) ont mis en place une feuille de route en 2020. La feuille de route a été mise à jour en 2022.
L’intégralité de la feuille de route est disponible ici : 
[Feuille de route pour la transition énergétique en Ile-de-France](https://www.drieat.ile-de-france.developpement-durable.gouv.fr/transition-energetique-les-priorites-de-l-etat-et-a4277.html){target="_blank"}

##  Le rôle des collectivités
### Renforcement des compétences des collectivités territoriales
Suivant la logique de territorialisation des compétences des lois de Modernisation de l’action publique territoriale et d’affirmation des métropoles (MAPTAM) et de Nouvelle organisation de la République (NOTRe), l’art. 188 de la Loi de Transition Énergétique pour la Croissance Verte (LTECV) renforce les compétences des collectivités territoriales en matière climat-air-énergie.
La transcription des objectifs d’une collectivité passe par la définition d’un Plan Climat Air Énergie Territorial (PCAET). Ceux-ci sont obligatoires pour les EPCI à fiscalité propre regroupant plus de 20 000 habitants.
Une [FAQ sur les PCAET](https://www.drieat.ile-de-france.developpement-durable.gouv.fr/les-pcaet-foire-aux-questions-a3444.html){target="_blank"} est disponible sur le site de la DRIEAT, et des informations sont également disponibles sur le [site de l’ADEME](https://territoireengagetransitionecologique.ademe.fr/pcaet-quest-ce-que-cest/#2.){target="_blank"} .
Plusieurs leviers d’action peuvent être mobilisés pour inciter l’adoption de mesures de rénovation énergétique du parc bâti.

### Des stratégies territoriales adaptées aux spécificités locales
L’Île-de-France est composée de territoires hétérogènes et dotés de parc de logements différents en termes de taille de logement, période de construction, types d’occupation (individuel, collectif, parc social), ou encore de niveau de ressources des habitants. De même, le parc tertiaire est inégalement réparti sur le territoire francilien, que ce soit en termes de typologie (bureaux, commerces, santé, sports et culture…), type de propriétaire (public, privé, …) ou période de construction.
Il est donc important de connaître les spécificités d’un territoire pour pouvoir définir une stratégie adaptée, et définir les secteurs sur lesquels agir en priorité.
Notons cependant que la majeure partie du parc bâti francilien est constituée de bâtiments construits avant 1988, date de la troisième réglementation thermique, qui introduit des valeurs de performance énergétique plus exigeantes. En conséquence, la problématique de rénovation énergétique du bâti est incontournable sur la plupart des territoires.
Bien connaître la répartition locale des compétences entre les différents niveaux de collectivités est primordial pour couvrir la problématique de rénovation énergétique et pour réaliser une stratégie de développement économique et sociale territoriale inclusive et transversale.
