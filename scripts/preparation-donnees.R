#  ------------------------------------------------------------------------
#
# Title : Batistato - Préparation données 
#    By : DRIEAT
#  Date : 2023-02
#
#  ------------------------------------------------------------------------

library(readr)

# Données energie
bat_energie <- read_csv("inputs/bat_energie.csv")
bat_energie <- janitor::clean_names(bat_energie)
bat_energie <- bat_energie %>% 
  rename(consommation = conso) %>% 
  mutate(
    consommation = as.numeric(consommation),
    energie = case_when(
      energie == "GN" ~ "Gaz naturel",
      energie == "PP" ~ "Charbon et Produit pétroliers",
      energie == "ELEC" ~ "Electricité",
      energie == "CU" ~ "Chauffage urbain",
      energie == "BOIS" ~ "Bois"
      #TRUE ~ "Pas d'info"
    ),
    usage = case_when(
      type_usage == "ECS" ~ "Eau chaude sanitaire",
      type_usage == "CHF" ~ "Chauffage",
      type_usage %in% c("AUTRES", "AUTRE") ~ "Autre usages spécifiques"
      #TRUE ~ "Pas d'info"
    ),
    type_bati = case_when(
      type_bati == "APPT" ~ "Appartement",
      type_bati == "MI" ~ "Maison",
      type_bati == "BRX" ~ "Bureaux",
      type_bati == "CAHORE" ~ "Cafés, hôtels, restaurants",
      type_bati == "COM" ~ "Commerces",
      type_bati == "ENSGT" ~ "Enseignement",
      type_bati == "HABCOM" ~ "Hâbitats communautaires",
      type_bati == "SANTESOC" ~ "Santé",
      type_bati == "SPORTLOIS" ~ "Sport et loisirs",
      type_bati == "TR" ~ "Bâti transport"
    ),
    secteur_format = case_when(
      secteur %in% c("RES") ~ "Résidentiel",
      secteur %in% c("TER") ~ "Tertiaire",
      TRUE ~"Autres (Transports routiers, Industrie et agriculture)"
    )
  ) %>% 
  mutate(
    energie = factor(
      x = energie,
      #ordered = TRUE,
      levels = c("Gaz naturel", "Electricité", "Charbon et Produit pétroliers", "Chauffage urbain", "Bois")
    )
  ) %>% 
  mutate(
    siren_epci = na_if(siren_epci, "NULL"),
    dep_com = na_if(dep_com, "NULL"),
    idcom = na_if(idcom, "NULL")
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE)
bat_energie$code_territoire[bat_energie$maille == "REGION"] <- "11"
saveRDS(bat_energie, file = "datas/bat_energie.rds")

# Données energie logement par période de construction
bat_energie_age <- read_csv("inputs/bat_energie_age.csv")
bat_energie_age <- janitor::clean_names(bat_energie_age)
bat_energie_age <- bat_energie_age %>% 
  rename(consommation = conso) %>% 
  mutate(
    consommation = as.numeric(consommation),
    type_bati = case_when(
      type_bati == "APPT" ~ "Appartement",
      type_bati == "MI" ~ "Maison",
      type_bati == "BRX" ~ "Bureaux",
      type_bati == "CAHORE" ~ "Cafés, hôtels, restaurants",
      type_bati == "COM" ~ "Commerces",
      type_bati == "ENSGT" ~ "Enseignement",
      type_bati == "HABCOM" ~ "Hâbitats communautaires",
      type_bati == "SANTESOC" ~ "Santé",
      type_bati == "SPORTLOIS" ~ "Sport et loisirs",
      type_bati == "TR" ~ "Bâti transport"
    ),
    secteur_format = case_when(
      secteur %in% c("RES") ~ "Résidentiel",
      secteur %in% c("TER") ~ "Tertiaire",
      TRUE ~"Autres (Transports routiers, Industrie et agriculture)"
    )
  ) %>% 
  mutate(
    siren_epci = na_if(siren_epci, "NULL"),
    dep_com = na_if(dep_com, "NULL"),
    idcom = na_if(idcom, "NULL")
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE)

bat_energie_age$age <- factor(
  x = bat_energie_age$age,
  levels = c("Avant 1946", "1946-1970", "1971-1990", "1991-2005", "Après 2005")
) 
bat_energie_age$code_territoire[bat_energie_age$maille == "REGION"] <- "11"
saveRDS(bat_energie_age, file = "datas/bat_energie_age.rds")

# Données energie tertiaire par énergie
bat_energie_ter <- read_csv("inputs/bat_energie_ter.csv")
bat_energie_ter <- janitor::clean_names(bat_energie_ter)
bat_energie_ter <- bat_energie_ter %>% 
  rename(consommation = conso) %>% 
  mutate(
    consommation = as.numeric(consommation),
    energie = case_when(
      energie == "GN" ~ "Gaz naturel",
      energie == "PP" ~ "Charbon et Produit pétroliers",
      energie == "ELEC" ~ "Electricité",
      energie == "CU" ~ "Chauffage urbain",
      energie == "BOIS" ~ "Bois"
      #TRUE ~ "Pas d'info"
    ),
    secteur_format = case_when(
      secteur %in% c("RES") ~ "Résidentiel",
      secteur %in% c("TER") ~ "Tertiaire",
      TRUE ~"Autres (Transports routiers, Industrie et agriculture)"
    )
  ) %>% 
  mutate(
    energie = factor(
      x = energie,
      #ordered = TRUE,
      levels = c("Gaz naturel", "Electricité", "Charbon et Produit pétroliers", "Chauffage urbain", "Bois")
    )
  ) %>% 
  mutate(
    siren_epci = na_if(siren_epci, "NULL"),
    dep_com = na_if(dep_com, "NULL"),
    idcom = na_if(idcom, "NULL")
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE)
bat_energie_ter$code_territoire[bat_energie_ter$maille == "REGION"] <- "11"
saveRDS(bat_energie_ter, file = "datas/bat_energie_ter.rds")


# Données logement
bat_logement <- read_csv("inputs/bat_logement.csv")
bat_logement$age <- factor(
  x = bat_logement$age,
  levels = c("Avant 1948", "1949-1974", "1975-1990", "1991-2005", "Après 2005")
)
bat_logement$type_occupant <- as.factor(bat_logement$type_occupant)
bat_logement$type_occupant_d <- as.factor(bat_logement$type_occupant_d)
bat_logement <- bat_logement %>% 
  mutate(
    nombre = as.numeric(nombre),
    surface = as.numeric(surface)
  ) %>% 
  mutate(
    siren_epci = na_if(siren_epci, "NULL"),
    dep_com = na_if(dep_com, "NULL"),
    idcom = na_if(idcom, "NULL")
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE)
bat_logement$code_territoire[bat_logement$maille == "REGION"] <- "11"
saveRDS(bat_logement, file = "datas/bat_logement.rds")

# Données logement détaillées
bat_logement_d <- read_csv("inputs/bat_logement_d.csv")
bat_logement_d$age <- factor(
  x = bat_logement_d$age,
  levels = c("Avant 1948", "1949-1974", "1975-1990", "1991-2005", "Après 2005")
)
bat_logement_d$type_occupant <- as.factor(bat_logement_d$type_occupant)
bat_logement_d$type_occupant_d <- as.factor(bat_logement_d$type_occupant_d)
bat_logement_d <- bat_logement_d %>% 
  mutate(
    nombre = as.numeric(nombre),
    surface = as.numeric(surface)
  ) %>% 
  mutate(
    siren_epci = na_if(siren_epci, "NULL"),
    dep_com = na_if(dep_com, "NULL"),
    idcom = na_if(idcom, "NULL")
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE)
bat_logement_d$code_territoire[bat_logement_d$maille == "REGION"] <- "11"
saveRDS(bat_logement_d, file = "datas/bat_logement_d.rds")

# Données logement non détaillées
bat_logement_nd <- read_csv("inputs/bat_logement_nd.csv")
bat_logement_nd$age <- factor(
  x = bat_logement_nd$age,
  levels = c("Avant 1948", "1949-1974", "1975-1990", "1991-2005", "Après 2005")
)
bat_logement_nd$type_occupant <- as.factor(bat_logement_nd$type_occupant)
bat_logement_nd <- bat_logement_nd %>% 
  mutate(
    nombre = as.numeric(nombre),
    surface = as.numeric(surface)
  ) %>% 
  mutate(
    siren_epci = na_if(siren_epci, "NULL"),
    dep_com = na_if(dep_com, "NULL"),
    idcom = na_if(idcom, "NULL")
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE)
bat_logement_nd$code_territoire[bat_logement_nd$maille == "REGION"] <- "11"
saveRDS(bat_logement_nd, file = "datas/bat_logement_nd.rds")


# Données tertiaire
bat_ter <- read_csv("inputs/bat_ter.csv")
bat_ter <- bat_ter %>% 
  mutate(
    nombre = as.numeric(nombre),
    surface = as.numeric(surface)
    ) %>% 
  mutate(
    assujetti = case_when(
      #assujetti == "?" ~ "à confirmer",
      assujetti == "O" ~ "Oui",
      assujetti == "N" ~ "Non",
      TRUE ~ "Pas d'info"
    )
  ) %>% 
  mutate(
    age = factor(
      x = age,
      levels = c("Avant 1948", "1949-1974", "1975-1990", "1991-2005", "Après 2005", "Inconnu")
    )
  ) %>% 
  mutate(
    categorie = factor(
      x = categorie,
      levels = c("Autre", "Bureaux", "Cafés, hôtels, restaurants", "Commerce", "Enseignement", 
                 "Habitat communautaire", "Santé", "Sport, Loisirs, Culture")
    )
  ) %>% 
  mutate(
    siren_epci = na_if(siren_epci, "NULL"),
    dep_com = na_if(dep_com, "NULL"),
    idcom = na_if(idcom, "NULL")
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE)
bat_ter$code_territoire[bat_ter$maille == "REGION"] <- "11"
saveRDS(bat_ter, file = "datas/bat_ter.rds")


# Données tertiaire assujetti
bat_ter_assujetti <- read_csv("inputs/bat_ter_a.csv")
bat_ter_assujetti <- bat_ter_assujetti %>% 
  mutate(
    nombre = as.numeric(nombre),
    surface = as.numeric(surface)
  ) %>% 
  mutate(
    siren_epci = na_if(siren_epci, "NULL"),
    dep_com = na_if(dep_com, "NULL"),
    idcom = na_if(idcom, "NULL")
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE)
bat_ter_assujetti$code_territoire[bat_ter_assujetti$maille == "REGION"] <- "11"
saveRDS(bat_ter_assujetti, file = "datas/bat_ter_assujetti.rds")


# Données dpe
bat_dpe <- read_csv("inputs/bat_dpe.csv")
bat_dpe <- janitor::clean_names(bat_dpe)
bat_dpe <- bat_dpe %>% 
  mutate(
    siren_epci = na_if(siren_epci, "NULL"),
    dep_com = na_if(dep_com, "NULL"),
    idcom = na_if(idcom, "NULL")
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE)
bat_dpe$code_territoire[bat_dpe$maille == "REGION"] <- "11"
saveRDS(bat_dpe, file = "datas/bat_dpe.rds")
