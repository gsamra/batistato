
#' Afficher le contenu d'un document markdown dans une page
#'
#' @param id ID du module
#' @param md Chemin vers le fichier markdown
#'
#' @name page-markdown
#' @export
#'
page_markdown_ui <- function(id) {
  ns <- NS(id)
  tagList(
    tags$div(
      style = css(maxWidth = "1200px", margin = "auto"),
      uiOutput(outputId = ns("contenu"))
    )
  )
}

#' @rdname page-markdown
#' @export
page_markdown_server <- function(id, md) {
  moduleServer(
    id = id,
    module = function(input, output, session) {
      
      output$contenu <- renderUI({
        tmp <- tempfile(fileext = ".html")
        render(
          input = md,
          output_file = tmp,
          output_format = html_fragment(),
          envir = globalenv(), 
          knit_root_dir = ".",
          params = list(),
          quiet = TRUE
        )
        includeHTML(tmp)
      })
      
    }
  )
}
