---
title: "Méthodologie"
---

# Bases de données utilisées :

BâtiSTATO se base sur 5 bases de données principales :
 
![](images/methodologie_tableau.png)


## Les Fichiers fonciers enrichis

A partir des données « MAJIC » de la Direction Générale des Finances Publiques (DGFiP) le CEREMA retraite, géolocalise et enrichit les Fichiers fonciers. Pour en savoir plus: [datafoncier.cerema.fr](https://datafoncier.cerema.fr/donnees/fichiers-fonciers){target="_blank"}.

### Millésime :

Le millésime actuellement utilisé est le millésime 2021.

### Méthode :

Ces données sont ensuite agrégées au niveau de la commune par un calcul DRIEAT/SEB, puis agrégées au niveau EPCI/Département/Région à partir des données sur la commune.
Le secret statistique est géré par un calcul DRIEAT/SEB. Seules les données concernant au moins 11 logements sur une commune sont affichées. Pour gérer le secret statistique de niveau 2, dans le cas où au niveau d’un EPCI/EPT ou d’un département la donnée serait facilement reconstituable, une seconde commune peut être secrétisée bien que la donnée concerne plus de 11 logements.

### Principaux biais :

Les données sont issues des fichiers DGFip pour les personnes assujetties à la taxe foncière. En conséquence les personnes non assujetties à la taxe foncière, et donc tout le parc social y est sous-représenté. C’est pourquoi il a été décidé de se baser sur RPLS pour décrire le parc social.


## Le Répertoire des logements locatifs des bailleurs sociaux (RPLS)

Le répertoire des logements locatifs des bailleurs sociaux a pour objectif de dresser l’état global du parc de logements locatifs de ces bailleurs sociaux au 1er janvier d’une année. Mis en place au 1er janvier 2011, il est alimenté par les informations transmises par les bailleurs sociaux.
Pour en savoir plus: [repertoire-des-logements-locatifs-des-bailleurs-sociaux](https://www.data.gouv.fr/fr/datasets/repertoire-des-logements-locatifs-des-bailleurs-sociaux/){target="_blank"}.

### Millésime :

Le millésime actuellement utilisé est le millésime 2022 (données au 1er janvier 2022).

### Méthode de calcul :

Ces données sont ensuite agrégées au niveau de la commune par un calcul DRIEAT/SEB, puis agrégées au niveau EPCI/Département/Région à partir des données sur la commune.
Les données présentées dans BATISTATO issues de RPLS ne sont pas soumises au secret statistique.


## La base de données des assujettis au décret éco-énergie tertiaire (EET) :

A partir des fichiers fonciers enrichis, le CEREMA croise avec les données de la Base Sirene des entreprises et de leurs établissements afin d’identifier les locaux tertiaires et leur assujettissement. Pour palier à la sous-représentation du parc public, cette base a été enrichie des données Etablissements recevant du public vulnérable (ERPV).

Ces données sont ensuite agrégées au niveau de la commune par un calcul DRIEAT/SEB, puis agrégées au niveau EPCI/Département/Région à partir des données sur la commune.
Lien vers pdf [« EET2021.pdf »](documents/EET2021.pdf){target="_blank"}.

### Millésime :

Le millésime actuellement utilisé est le millésime 2021.

### Méthode de calcul :

Un traitement DRIEAT a été fait pour supprimer les doublons issus des fichiers fonciers et de l’enrichissement ERPV.

### Secret statistique :

Le secret statistique est géré par un calcul DRIEAT/SEB. Seules les données concernant au moins 11 locaux sur une commune sont affichées. Pour gérer le secret statistique de niveau 2, dans le cas où au niveau d’un EPCI/EPT ou d’un département la donnée serait facilement reconstituable, une seconde commune peut être secrétisée bien que la donnée concerne plus de 11 locaux.

### Principaux biais :

Les données sont issues des fichiers DGFip pour les personnes assujetties à la taxe foncière. En conséquence les personnes non assujetties à la taxe foncière, et donc tout le parc public y est sous-représenté, ou mal représenté.
Lorsque les données proviennent uniquement de l’enrichissement ERPV, la surface de plancher a été estimée à partir de l’emprise au sol et du nombre d’étages, ce qui donne une estimation très approximative.


## Les données ENERGIF du ROSE

### Millésime :

Le millésime actuellement utilisé est le millésime 2019.

### Méthode de calcul :

La méthodologie est détaillée dans le document ci-dessous :
Lien vers le pdf [« 20220901_AIRPARIF_Methode_ENERGIF.pdf »](documents/20220901_AIRPARIF_Methode_ENERGIF.pdf){target="_blank"}.

Ces données sont une modélisation harmonisée de Airparif pour le ROSE. Toutes les données sont exprimées en MWh et représentent la consommation d’énergie finale. Ce sont les données climat réel, non corrigées des variations climatiques, qui ont été utilisées.

### Principaux biais :

Ces données sont issues d’une modélisation.


## La Base de Données Nationale du Bâtiment (BDNB)

Les données sur la performance énergétique du parc sont issues de la modélisation du Centre Scientifique et Technique du Bâtiment (BDNB).

### Millésime :

2022.10.c

### Méthode de calcul :

Un traitement DRIEAT/SEB/DB somme ensuite les résultats à l’EPCI, Département, Région.

### Principal biais :

Ces données sont des modélisations. Seul le parc de logements a pour l’instant été modélisé.


# Axes d’analyses

## Logement, description du parc bâti :

### Année de construction

Les périodes de construction utilisées dans ces graphiques correspondent aux dates charnières au regard des évolutions des pratiques constructives et des réglementations thermiques.
 * 1948 : Date charnière pour l’évolution des techniques de construction
 * 1974 : RT 1974
 * 1988 : RT 1988
 * 2005 : RT 2005

### Type d’occupation

Parc social, Collectif privé, Individuel Privé

### Type d’occupation détaillée

Parc social, Collectif privé – Propriétaire Occupant, Collectif privé – Locatif, Individuel Privé – Propriétaire Occupant, Individuel Privé – Locatif.

Une stratégie de rénovation doit prendre en compte l’ensemble du parc, mais les stratégies diffèrent selon le type d’occupant, et le type de propriété.

Tout le parc de logement est considéré, il n’a pas été fait de distinction entre logement vacant, résidence principale et résidence secondaire.


## Tertiaire, description du parc bâti

### Type d’usage

Les catégories retenues sont celles du Centre d’Etudes et de Recherches Économiques sur l’Energie (CEREN). Ces catégories correspondent à celles utilisées pour la modélisation Airparif des données de consommation ENERGIF du ROSE.
Pour les données issues des fichiers fonciers enrichis, la catégorie a été recalculée à partir :

 *  du code NAF s’il était renseigné, en utilisant la répartition du CEREN
 *  à défaut à partir du code_categorie des fichiers fonciers selon le tableau suivant :


| Code_categorie fichiers fonciers | Catégorie dans BATISTATO | 
| :---------------|:---------------|
| BUR1, BUR2  |   Bureaux     | 
| CLI1,CLI2,BUR3  | Santé, Action sociale             |  
| ENS1,ENS2  | Enseignement          | 
| MAG1,MAG2,MAG3,MAG4,MAG5,MAG6,MAG7  | Commerce          |  
| SPE1,SPE2,SPE3,SPE4,SPE5,SPE6,SPE7  | Sport, Loisirs, Culture          |  
| HOT1,HOT2,HOT3  | Cafés, hôtels, restaurants          |  
| HOT4,HOT5,CLI3,CLI4  | Habitat communautaire          |  
<br>
Pour les données issues de l’enrichissement ERPV, la catégorisation a été faite à partir de mot-clefs présent dans le champ du nom du propriétaire :
<br>
<br>


| Mot clef présent dans le nom du propriétaire | Catégorie dans BATISTATO | 
| :---------------|:---------------|
| Hospitalier, CMP, HDJ, hopital, hôpital  |   Santé     | 
| Lycée, lycee, école, ecole, collège, college, université, universite  | Enseignement          |  
| Patinoire, pâtinoire, piscine, gymnase  | Sport, Loisirs, Culture          |  
<br>
Dans tous les autres cas, la catégorie « Autre » a été utilisée.

### Année de construction

Le même découpage a été retenu que pour la description du parc bâti logement.


# CC du Pays Houdanais

Pour la CC du Pays Houdanais, seules les données des communes situées en Ile-de-France sont utilisées.



