DO
$$
BEGIN

/* Le nom de la table finale table_communes_insee est à modifier dans tout le code */
/* Le nom de la table des locaux bdnb locaux_bdnb est à remplacer dans tout le code */ 
/* Le nom de la table des simulations dpe bdnb_dpe_simulation est à remplacer dans tout le code */ 
/* Le nom de la table finale table_finale_dpe est à modifier dans tout le code */

	-- 1. CREATION DE LA TABLE
/*
		-- supprimer la table si elle existe
		drop table if exists table_finale_dpe;

		-- créer la table 
		create table table_finale_dpe (
			maille			varchar(8) NOT NULL,
			idcom 			varchar(5) ,
			siren_epci		varchar(254),
			dep_com			varchar(254),
			secteur			varchar(10),
			type_bati		varchar(10),
			type_occupant	text,
			nb_a			numeric,
			nb_b			numeric,
			nb_c			numeric,
			nb_d			numeric,
			nb_e			numeric,
			nb_f			numeric,
			nb_g			numeric	
		);
*/
	-- 2. CALCUL DES DONNEES NIVEAU COMMUNE
		-- supprimer la table temporaire si elle existe
		DROP table if exists t_dpe;

		-- créer la table temporaire avec uniquement les variables nécessaires
		CREATE temporary table t_dpe
		as 
		SELECT l.libelle_commune as idcom,'RES' as secteur,
		round(sum(dpe.etiquette_dpe_initial_a::numeric),0) as nb_a,
		round(sum(dpe.etiquette_dpe_initial_b::numeric),0) as nb_b,
		round(sum(dpe.etiquette_dpe_initial_c::numeric),0) as nb_c,
		round(sum(dpe.etiquette_dpe_initial_d::numeric),0) as nb_d,
		round(sum(dpe.etiquette_dpe_initial_e::numeric),0) as nb_e,
		round(sum(dpe.etiquette_dpe_initial_f::numeric),0) as nb_f,
		round(sum(dpe.etiquette_dpe_initial_g::numeric),0) as nb_g
		FROM bdnb_dpe_simulation dpe
			INNER JOIN locaux_bdnb l 
				on dpe.local_id = l.local_id
		GROUP BY idcom
		;
		
		-- vider la table
		
		TRUNCATE table_finale_dpe;

		INSERT INTO table_finale_dpe
		SELECT 'COMMUNE' as maille,idcom,NULL as siren_epci,NULL as dep_com,
		'RES' as Secteur,NULL as type_bati,NULL as type_occupant,
		nb_a,nb_b,nb_c,nb_d,nb_e,nb_f,nb_g
		from t_dpe;

	-- 3. GROUPEMENT PAR EPCI, EPT, DEPARTEMENT

		delete from table_finale_dpe 
		where maille<>'COMMUNE';

		/*Communes et leurs EPCI hors département 28*/
		drop table if exists t_communes;
		create temporary table t_communes
		as select code_insee,siren_epci  -- EPCI
		from table_communes_insee
		where dep_com<>'28' and siren_epci is not null
		union
		select code_insee,id_ept as siren_epci -- EPT
		from table_communes_insee
		where dep_com<>'28' and id_ept is not null
		;

		drop table if exists t_dep;
		create temporary table t_dep
		as select code_insee,dep_com 
		from table_communes_insee
		where dep_com<>'28' 
		;

		/*Groupement maille EPCI*/
		drop table if exists t_bat_1;
		create temporary table t_bat_1
		as select 'EPCI' as maille, c.siren_epci,
		secteur,type_bati,type_occupant,
		sum(nb_a) as nb_a,sum(nb_b) as nb_b,sum(nb_c) as nb_c,
		sum(nb_d) as nb_d,sum(nb_e) as nb_e,sum(nb_f) as nb_f,sum(nb_g) as nb_g
		from table_finale_dpe  b_0 
			left join t_communes c
				on b_0.idcom=c.code_insee
		group by maille,c.siren_epci,secteur,type_bati,type_occupant;	

		/*Groupement maille Département*/
		drop table if exists t_bat_2;
		create temporary table t_bat_2
		as select 'DEP' as maille, c.dep_com as dep_com,
		secteur,type_bati,type_occupant,sum(nb_a) as nb_a,sum(nb_b) as nb_b,sum(nb_c) as nb_c,
		sum(nb_d) as nb_d,sum(nb_e) as nb_e,sum(nb_f) as nb_f,sum(nb_g) as nb_g
		from table_finale_dpe  b_0 
			inner join t_dep c
				on b_0.idcom=c.code_insee
		group by maille,c.dep_com,secteur,type_bati,type_occupant;

		/*Groupement maille Région*/
		drop table if exists t_bat_3;
		create temporary table t_bat_3
		as select 'REGION' as maille, secteur,type_bati,type_occupant,
		sum(nb_a) as nb_a,sum(nb_b) as nb_b,sum(nb_c) as nb_c,sum(nb_d) as nb_d,
		sum(nb_e) as nb_e,sum(nb_f) as nb_f,sum(nb_g) as nb_g
		from table_finale_dpe 
		group by secteur,type_bati,type_occupant
		;

		insert into table_finale_dpe
		select maille,NULL as idcom,siren_epci as siren_epci,NULL as dep_com,
		secteur,type_bati,type_occupant,
		nb_a,nb_b,nb_c,nb_d,nb_e,nb_f,nb_g  
		from t_bat_1;

		insert into table_finale_dpe
		select maille,NULL as idcom,NULL as siren_epci,dep_com as dep_com,
		secteur,type_bati,type_occupant,
		nb_a,nb_b,nb_c,nb_d,nb_e,nb_f,nb_g  
		from t_bat_2;

		insert into table_finale_dpe
		select maille,NULL as idcom,NULL as siren_epci,NULL as dep_com,
		secteur,type_bati,type_occupant,
		nb_a,nb_b,nb_c,nb_d,nb_e,nb_f,nb_g  
		from t_bat_3;
		
	-- 4. EXPORTER
	/*
	SELECT * FROM table_finale_dpe; 
	*/


END
$$

