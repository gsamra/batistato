---
title: "FAQ"
---

## FAQ


### Les données sont-elles disponibles pour toutes les communes d’Ile-de-France, quelle que soit leur taille ?

Oui, les données sont disponibles pour toutes les communes, EPCI, EPT, et départements d’Ile-de-France. Cependant, certaines données étant issues des fichiers fonciers, elles sont secrétisées (et donc non disponibles) lorsqu’il y a moins de 11 logements (pour les logements), ou moins de 11 locaux (pour le tertiaire) concernés.  
D’autre part, pour la CC du Pays Houdanais, dont certaines communes sont situées hors Ile-de-France, seules les données des communes étant en Ile-de-France sont disponibles.

### Des données sur le parc tertiaire public sont-elles disponibles ?

BATISTATO inclut des données sur le parc public, mais elles sont moins précises et moins exhaustives que pour le parc privé. Le parc tertiaire privé est décrit notamment grâce aux fichiers fonciers ; le parc public n’étant pas soumis à la taxe foncière, la connaissance de ce parc est moins précise. Pour parer à ce biais, le CEREMA a ajouté des données concernant les établissements recevant du public vulnérable (ERPV). À terme, la plateforme OPERAT, qui recense les données bâtimentaires et de consommations des bâtiments assujettis à ce dispositif, devrait permettre d’avoir une meilleure vision de ce parc.

### Pourquoi faire un focus sur le parc bâti construit avant 1990 ?

La RT 1988 est la première réglementation thermique à imposer des critères de performance énergétique ambitieux. Nous pouvons donc considérer que l’ensemble des bâtiments livrés avant 1990 sont à rénover. Cela ne signifie pas pour autant qu’aucune réflexion sur la performance des bâtiments post-1990 ne doit être envisagée.

### Pour le parc tertiaire, pourquoi un zoom n’est réalisé que sur les bureaux et commerces ?

Les principales catégories d’activités en termes de surfaces en Ile-de-France sont les bureaux, les commerces, la santé, l’enseignement et le sport/ les établissements culturels. Les données concernant le parc public étant moins précises et moins exhaustives, et le nombre et les surfaces de bâtiments de bureaux et commerces privés étant déjà considérable, nous avons pris le parti d’analyser plus en détails ces deux catégories.

### Quelles différences avec l’outil Energif ROSE ?

L’outil Energif du ROSE décrit tous les secteurs (résidentiel, tertiaire, agricole et industriel), Batistato est axé sur la rénovation énergétique et les indicateurs associés. Batistato est plus détaillé sur certains axes d’analyse (distinction propriétaire/ locataire par exemple), et utilise d’autres bases de données, en particulier les fichiers fonciers pour la description du parc bâti.

### Quelles bases de données sont utilisées ?
Voir l'onglet "Methodologie".

### Quelles sont les perspectives de développement de l’outil ?

L’application BATISTATO n’est pas figée. Une mise à jour régulière des données sera réalisée en fonction des mises à jours des producteurs de données. Ces données étant mises à jour au mieux annuellement, BATISTATO suivra ce rythme.

De nouveaux indicateurs et graphiques pourront être développés en fonction des besoins identifiés par les utilisateurs. Vous pouvez faire des propositions via le formulaire de contact.
