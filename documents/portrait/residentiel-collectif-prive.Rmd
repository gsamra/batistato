#### Focus : Collectif privé

Ce focus permet d’identifier les spécificités du parc de logements collectifs privés  du territoire sélectionné.



##### Statut d’occupation selon période de construction – Logement Collectif privé {.no-comparaison}

```{r, eval=un_seul}
data <- params$donnees_logement

g1 <- afficher_graphique_n_logements_par_periode_construction_collectif_prive(donnees = data)
g2 <- afficher_graphique_surface_par_periode_construction_collectif_prive(donnees = data)

apex_grid(
  g1, g2,
  ncol = 2
)
```

```{r}
bouton_afficher_info("Pour en savoir plus", cible_id = "aide-3")
```

```{r}
bouton_afficher_info(
  "Afficher les données",
  cible_id = ns("tableau-3"),
  icon = ph("table")
)
```



<!-- Bloc de texte masqué et affiché seulement si clic sur le bouton -->
:::::: {.collapse #aide-3}

::: {.card .card-body .mt-1}

**Aide à l'interprétation :**  

Une stratégie territoriale de rénovation énergétique doit être différenciée selon les périodes de construction des logements. En effet, les modes constructifs, les matériaux employés (pierre, brique, béton...) et les types architecturaux ont fortement évolué dans le temps, ce qui nécessite une adaptation des techniques de rénovation énergétique.

Les périodes de construction utilisées dans ces graphiques correspondent aux dates charnières au regard des évolutions des pratiques constructives et des réglementations thermiques.

**Questions pour aider à l'interprétation des graphiques:**  
Y-a-t-il une période de construction dominante qui se dégage ?  
Y-a-t-il une forte part de bâti avant 1948 (bâti dit « ancien ») ? Si oui, il y a un fort enjeu patrimonial, et il faudra apporter une vigilance particulière aux risques de malfaçons.  
Y-a-t-il des ensembles avec possibilité d’industrialisation et de réplicabilité d’un bâtiment à l’autre ?  


:::

:::::: 


```{r}
tags$div(
  class = "collapse",
  id = ns("tableau-3"),
  tableau_ui(ns("res_col_priv_tableau_nombre")),
  tableau_ui(ns("res_col_priv_tableau_surface"))
)
```





##### Statut d'occupation avant 1990 - Logement Collectif privé

Ces graphiques se limitent aux logements antérieurs à 1990, car le parc de logements nécessitant une rénovation énergétique est principalement constitué de logements construits avant cette date. Il existe aussi des logements construits après 1990, qui sont énergivores et nécessitent une rénovation énergétique, mais ils sont moins représentatifs en raison de l’ambition croissante des règlements thermiques. Les logements construits en 1990 doivent en effet respecter a minima la deuxième réglementation thermique (RT 1988).

```{r, eval=un_seul}
data <- params$donnees_logement

g1 <- afficher_graphique_n_logements_par_type_occupant_collectif_prive(donnees = data)
g2 <- afficher_graphique_surface_par_type_occupant_collectif_prive(donnees = data)

apex_grid(
  g1, g2,
  ncol = 2
)
```

```{r, eval=plusieurs}
data <- params$donnees_logement

g1 <- afficher_graphique_n_logements_par_type_occupant_collectif_prive_plusieurs(donnees = data)
g2 <- afficher_graphique_surface_par_type_occupant_collectif_prive_plusieurs(donnees = data)

apex_grid(
  g1, g2,
  ncol = 2
)
```


```{r}
bouton_afficher_info("Pour en savoir plus", cible_id = "aide-8")
```


```{r, eval=un_seul}
bouton_afficher_info(
  "Afficher les données",
  cible_id = ns("tableau-27"),
  icon = ph("table")
  )
tags$div(
  class = "collapse",
  id = ns("tableau-27"),
  tableau_ui(ns("res_collectif_tableau_statut_n")),
  tableau_ui(ns("res_collectif_tableau_statut_surface"))
)
```


```{r, eval=plusieurs}
bouton_afficher_info(
  "Afficher les données",
  cible_id = ns("tableau-28"),
  icon = ph("table")
  )
tags$div(
  class = "collapse",
  id = ns("tableau-28"),
  tableau_ui(ns("res_collectif_tableau_statut_plusieurs_n")),
  tableau_ui(ns("res_collectif_tableau_statut_plusieurs_surface"))
)
```



<!-- Bloc de texte masqué et affiché seulement si clic sur le bouton -->
:::::: {.collapse #aide-8}

::: {.card .card-body .mt-1}

**Aide à l'interprétation :**

Ces graphiques permettent d'identifier comment est structuré le parc de logements collectifs privés du territoire considéré, en nombre et en surface, selon le statut d'occupation (propriétaire occupant, locataire).

Une stratégie territoriale de rénovation énergétique doit en effet proposer des mesures adaptées à chaque segment du parc. Compte-tenu de l'ampleur des enjeux, même s'il ne représente que 10% du parc, aucun segment ne peut être négligé.

La segmentation Individuel privé / Collectif privé / Parc social correspond à la segmentation retenue pour la définition des objectifs de rénovation énergétique des logements du Schéma régional du climat, de l'air et de l'énergie (SRCAE) francilien.

**Questions pour aider à l'interprétation des graphiques:**  
Quel est le statut d'occupation majoritaire?

:::

:::::: 


