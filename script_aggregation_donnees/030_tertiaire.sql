DO
$$
BEGIN

/* Le nom de la table finale table_communes_insee est à modifier dans tout le code */
/* Le nom de la table de la base éco-énergie tertiaire table_beet est à remplacer dans tout le code */ 
/* Le nom de la table finale table_finale_tertiaire est à modifier dans tout le code */
/* Le nom de la table finale table_finale_tertiaire_a est à modifier dans tout le code */


/*
	--1. CREER LES TABLES
	-- supprimer la table si elle existe
	drop table if exists table_finale_tertiaire;
	drop table if exists table_finale_tertiaire_a;

	-- créer la table 
	create table table_finale_tertiaire (
		maille			varchar(8) NOT NULL,
		idcom 			varchar(5) ,
		siren_epci		varchar(254),
		dep_com			varchar(254),
		assujetti		varchar(1),
		age				text,
		categorie		text,
		nombre			numeric,
		surface			numeric,
		secret_f		char(1)
	);
	
	create table table_finale_tertiaire_a (
		maille				varchar(8) NOT NULL,
		idcom 				varchar(5) ,
		siren_epci			varchar(254),
		dep_com				varchar(254),
		categorie_surface	text,
		nombre				numeric,
		surface				numeric,
		secret_f		char(1)
	);
*/
	-- 2. CALCULS A LA COMMUNE
	-- supprimer la table temporaire si elle existe
	drop table if exists t_b_ter_ff;

	-- créer la table temporaire avec uniquement les variables nécessaires
	create temporary table t_b_ter_ff
	as 
	select assujetti,idtup,ccodep,idcom,code_categorie,jannat,idban,dsup1,1 as nombre_local,catpro3,nom_prop,voirie,code_naf
	from table_beet
	where tertiaire='O' and code_categorie is not null -- issu fichiers fonciers
	;
	
	-- groupement à la TUP des données issues des fichiers fonciers
	drop table if exists t_b_ter_ff_group;
	create temporary table t_b_ter_ff_group
	as 
	select idtup,min(ccodep) as ccodep,min(idcom) as idcom,sum(dsup1) as dsup1,min(nom_prop) as nom_prop,min(voirie)as voirie,
	min(catpro3) as catpro3,min(jannat) as jannat
	from t_b_ter_ff
	group by idtup
	;
	
	drop table if exists t_b_ter_erp;
	-- créer la table temporaire avec uniquement les variables nécessaires
	create temporary table t_b_ter_erp
	as 
	select eet.idtup,ff.idtup as idtup_ff,coalesce(eet.ccodep,ff.ccodep) as ccodep,coalesce(eet.idcom,ff.idcom) as idcom,
	case when (eet.dsup1-ff.dsup1)/ff.dsup1*100<0.1 then 0 
	when ff.dsup1 is null then eet.dsup1
	when (eet.dsup1-ff.dsup1)>0 then eet.dsup1-ff.dsup1 
	else NULL end as dsup1,1 as nombre_local,
	case when eet.nom_prop is null or eet.nom_prop='NR' then ff.nom_prop
	else eet.nom_prop end as nom_prop,coalesce(eet.jannat,ff.jannat) as jannat,
	eet.idban,ff.catpro3 as catpro3_ff,eet.catpro3 as catpro3_erp,assujetti,eet.dsup1 as dsup1_eet
	from table_beet eet
		left join t_b_ter_ff_group ff
			on eet.idtup=ff.idtup
	where eet.tertiaire='O' and eet.code_categorie is null -- issu erp
	;
	
	-- Calcul des codes catégories ff
	DROP TABLE IF EXISTS t_b_ter_ff_1;
	CREATE TEMPORARY TABLE t_b_ter_ff_1
	as
	SELECT
	assujetti,ccodep,idcom,jannat,idban,dsup1,1 as nombre_local,catpro3,nom_prop,
	case  when code_naf in ('5510Z', '5610A', '5610C', '5610B','5621Z', '5629A', '5629B', '5630Z') then 'Cafés, hôtels, restaurants'
		 when code_naf in ('5520Z', '5530Z', '5590Z', '8422Z', '8710A', '8730A', '8423Z', '8810A', '9491Z') then 'Habitat communautaire'
		when code_naf in ('8610Z', '8621Z', '8622A', '8622B', '8622C', '8690C', '8710B', '8710C', '8720A', '8720B', '8730B', '8790A', '8790B',
'8810B', '8810C', '8891A', '8891B', '8899A', '8899B') then 'Santé'
		when code_naf in ('7211Z', '7219Z', '7220Z', '8510Z', '8520Z', '8531Z', '8532Z', '8531Z', '8532Z', '8541Z', '8542Z', '8552Z',
'8559A', '8559B') then 'Enseignement'
		when code_naf in ('3600Zd', '3700Z', '4939C', '3811Z', '3812Z', '3821Z', '3822Z', '5911A', '5911B', '5911C', '5912Z', '5913A', '5913B',
'5914Z', '6010Z', '6020A', '6020B', '8551Z', '9001Z', '9002Z', '9003A', '9003B', '9004Z', '9102Z', '9103Z', '9104Z',
'9200Z', '9311Z', '9312Z', '9319Z', '9321Z', '9329Z') then 'Sport, Loisirs, Culture'
	when code_naf in ('1013B', '1071B', '1071C', '1071D', '4511Z', '4519Z', '4520A', '4520B', '4531Z', '4532Z', '4540Z' , '4621Z', '4622Z',
'4623Z', '4624Z', '4631Z', '4632A', '4632B', '4632C', '4633Z', '4634Z', '4635Z', '4636Z', '4637Z', '4638A', '4638B',
'4639A', '4639B', '4641Z', '4642Z', '4643Z', '4644Z', '4645Z', '4646Z', '4647Z', '4648Z', '4649Z', '4651Z','4652Z',
'4661Z', '4662Z', '4663Z', '4664Z', '4665Z', '4666Z', '4669A', '4669B', '4669C', '4671Z', '4672Z', '4673A','4673B',
'4674A', '4674B', '4675Z', '4676Z', '4677Z', '4690Z','4711D','4711E','4711F','4711A', '4711B', '4711C','4719A',
'4719B','4721Z','4722Z','4723Z','4724Z','4725Z','4726Z', '4729Z', '4730Z', '4741Z', '4742Z', '4743Z', '4751Z', '4752A',
'4752B', '4753Z', '4754Z', '4759A', '4759B', '4761Z', '4762Z', '4763Z', '4764Z', '4765Z', '4771Z', '4772A','4772B',
'4773Z', '4774Z', '4775Z', '4776Z', '4777Z', '4778A', '4778B', '4778C', '4779Z', '4782Z', '4789Z', '4791A', '4791B',
'4799A', '4799B','4781Z','5210A','5210B','7420Z','8292Z','9313Z','9511Z','9512Z','9521Z','9522Z', '9523Z',
'9524Z', '9525Z', '9529Z', '9601A', '9601B', '9602A', '9602B', '9603Z', '9604Z', '9609Z') then 'Commerce'
when code_naf in ('4110A', '4110B', '4110C', '4110D', '4299Z', '4611Z', '4612A', '4612B', '4613Z', '4614Z', '4615Z', '4616Z', '4617A',
'4617B', '4618Z', '4619A', '4619B', '5224A', '5224B', '5229A', '5229B', '5310Z', '5320Z', '5812Z', '5821Z', '5829A',
'5829B', '5829C', '6110Z', '6120Z', '6130Z', '6190Z', '6202A', '6202B', '6203Z', '6209Z', '6201Z', '6311Z', '6312Z'
'6391Z', '6399Z', '6411Z', '6419Z', '6420Z', '6430Z', '6491Z', '6492Z', '6499Z', '6511Z', '6512Z', '6520Z', '6630Z',
'6530Z', '6611Z', '6612Z', '6619A', '6619B', '6621Z', '6622Z', '6629Z', '6810Z', '6820A', '6820B', '6831Z', '6832A',
'6832B', '6910Z', '6920Z', '7010Z', '7021Z', '7022Z', '7111Z', '7112A', '7112B', '7120A', '7120B', '7311Z', '7312Z',
'7320Z', '7410Z', '7430Z', '7490A', '7490B', '7500Z', '7711A', '7711B', '7712Z', '7721Z', '7722Z', '7729Z', '7731Z',
'7732Z', '7733Z', '7734Z', '7735Z', '7739Z', '7740Z', '7810Z', '7820Z', '7830Z', '7911Z', '7912Z', '7990Z', '8010Z',
'8020Z', '8030Z', '8110Z', '8121Z', '8122Z', '8129A', '8129B', '8130Z', '8211Z', '8219Z', '8220Z', '8230Z', '8291Z',
'8299Z', '8411Z', '8412Z', '8413Z', '8421Z', '8423Z', '8424Z', '8425Z', '8430A', '8430B',
'8430C', '8553Z', '8560Z', '8623Z', '8690A', '8690B', '8690D', '8690E', '8690F', '9101Z', '9411Z', '9412Z', '9420Z',
'9492Z', '9499Z', '9900Z') then 'Bureaux'
	when code_categorie in ('BUR1','BUR2') then 'Bureaux'
		  when code_categorie in ('CLI1','CLI2','BUR3') then 'Santé'
		  when code_categorie in ('ENS1','ENS2') then 'Enseignement'
		  when code_categorie in ('MAG1','MAG2','MAG3','MAG4','MAG5','MAG6','MAG7') 
		  and code_naf in ('5510Z','5610A','5610B','561CB','5612Z','5629A','5629B','5630Z') then 'Cafés, hôtels, restaurants'
		  when code_categorie in ('MAG1','MAG2','MAG3','MAG4','MAG5','MAG6','MAG7') then 'Commerce'
		  when code_categorie in ('SPE1','SPE2','SPE3','SPE4','SPE5','SPE6','SPE7') then 'Sport, Loisirs, Culture'
		  when code_categorie in ('HOT1','HOT2','HOT3') then 'Cafés, hôtels, restaurants'
		  when code_categorie in ('HOT4','HOT5','CLI3','CLI4') then 'Habitat communautaire'
		  else 'Autre' end as categorie
		  --case when left(catpro3,1)<>'P' then 'Privé' else 'Public' end as public_prive
	from
	t_b_ter_ff
	;
	
	-- Calcul des codes catégories erpv
	DROP TABLE IF EXISTS t_b_ter_erp_1;
	CREATE TEMPORARY TABLE t_b_ter_erp_1
	as
	SELECT
	assujetti,ccodep,idcom,jannat,idban,dsup1,1 as nombre_local,nom_prop,
	case when upper(nom_prop) like '%HOSPITALIER%'  or  UPPER(nom_prop) like '%HDJ%' 
		 or lower(nom_prop) like '%hopital%' or lower(nom_prop) like '%hôpital%'  or UPPER(nom_prop) like '%CMP%' then 'Santé'
			when (LOWER(nom_prop) like '%lycée%' or LOWER(nom_prop) like '%lycee%'
			or LOWER(nom_prop) like '%école%' or LOWER(nom_prop) like '%ecole%'
			or LOWER(nom_prop) like '%collège%' or LOWER(nom_prop) like '%college%'
			or LOWER(nom_prop) like '%université%' or LOWER(nom_prop) like '%universite%') then 'Enseignement'
		when (LOWER(nom_prop) like '%patinoire%' or LOWER(nom_prop) like '%pâtinoire%'
			or LOWER(nom_prop) like '%piscine%' or lower(nom_prop) like '%gymnase%') then 'Sport, Loisirs, Culture'
		when LOWER(nom_prop) like '%ehpad%' or lower(nom_prop) like '%residence%'
		or lower(nom_prop) like '%foyer%' then 'Habitat communautaire'
		when idban like 'ENSGNT%' or catpro3_erp like 'ENSGNT%' then 'Enseignement'
		when idban like 'SANTEFIN%' or catpro3_erp like 'SANTEFIN%'  then 'Santé'
		when idban like 'PAISANTE%' or catpro3_erp like 'PAISANTE%' then 'Santé'
		when idban like 'PAISPORT%' or catpro3_erp like 'PAISPORT%' then 'Sport, Loisirs, Culture'
		when idban like 'SPORTRES%' or catpro3_erp like 'SPORTRES%' then 'Sport, Loisirs, Culture'
	  	else 'Autre' end as categorie
		-- case when left(catpro3_ff,1)<>'P' then 'Privé' else 'Public' end as public_prive
	from t_b_ter_erp
	where dsup1>0 -- Elimination des doublons
	;
	
	DROP TABLE IF EXISTS t_b_ter_0;
	CREATE TEMPORARY TABLE t_b_ter_0
	as
	SELECT ccodep,idcom,categorie,jannat,nombre_local,dsup1,assujetti,nom_prop
	FROM t_b_ter_ff_1
	union
	SELECT ccodep,idcom,categorie,jannat,nombre_local,dsup1,assujetti,nom_prop
	FROM t_b_ter_erp_1
	;
	

	-- catégorie d'années, catégorie de surface, catégorie d'assujetissement
	drop table if exists t_b_ter_1;
	create temporary table t_b_ter_1
	as select ccodep,idcom,categorie,
	case when jannat <= 1948 then 'Avant 1948'
		when jannat <= 1974 then '1949-1974'
		when jannat <= 1990 then '1975-1990'
		when jannat <= 2005 then '1991-2005'
		when jannat > 2005 then 'Après 2005'
		else 'Inconnu' end as age,
		case when dsup1 <= 100 then '0 - 100m²'
		when dsup1 <= 1000 then '100 - 1000m²'
		when dsup1 > 1000 then '>1000m²' 
		else 'Inconnu' end as categorie_surface,nombre_local,dsup1,
		case when assujetti='O' then 'O' else 'N' end as assujetti
	from t_b_ter_0
	;
	
	drop table if exists t_bat_ter;
	create temporary table t_bat_ter
	as select idcom,assujetti,age,categorie,sum(nombre_local) as nombre,sum(dsup1) as surface
	from t_b_ter_1
	group by idcom,assujetti,age,categorie;
	
	truncate table_finale_tertiaire;
	insert into table_finale_tertiaire
	select 'COMMUNE' as maille,idcom,NULL as siren_epci,null as dep_com,
	assujetti,age,categorie,nombre,surface
	from t_bat_ter;

	drop table if exists t_bat_ter_a;
	create temporary table t_bat_ter_a
	as select idcom,categorie_surface,sum(nombre_local) as nombre,sum(dsup1) as surface
	from t_b_ter_1
	where assujetti='O'
	group by idcom,categorie_surface;

	truncate table_finale_tertiaire_a;
	insert into table_finale_tertiaire_a
	select 'COMMUNE' as maille,idcom,NULL as siren_epci,null as dep_com,
	categorie_surface,nombre,surface
	from t_bat_ter_a;

	-- 3. GROUPEMENT EPCI, DEPARTEMENT, REGION
	DELETE
	FROM table_finale_tertiaire
	WHERE maille<>'COMMUNE';
	
	DELETE
	FROM table_finale_tertiaire_a 
	WHERE maille<>'COMMUNE';
	
	/*Communes et leurs EPCI hors département 28*/
	drop table if exists t_communes;
	create temporary table t_communes
	as select code_insee,siren_epci  -- EPCI
	from table_communes_insee
	where dep_com<>'28' and siren_epci is not null
	union
	select code_insee,id_ept as siren_epci -- EPT
	from table_communes_insee
	where dep_com<>'28' and id_ept is not null
	; -- CC du pays Houdanais, Région Ile de France et département 28
	
	drop table if exists t_dep;
	create temporary table t_dep
	as select code_insee,dep_com 
	from table_communes_insee
	where dep_com<>'28' 
	;

	drop table if exists t_bat_1;
	create temporary table t_bat_1
	as select 'EPCI' as maille, c.siren_epci as siren_epci,
	assujetti,age,categorie,sum(nombre) as nombre,sum(surface) as surface
	from table_finale_tertiaire b_0 
		left join t_communes c
			on b_0.idcom=c.code_insee
	group by maille,c.siren_epci,assujetti,age,categorie;
 
 	drop table if exists t_bat_2;
	create temporary table t_bat_2
	as select 'DEP' as maille, c.dep_com as dep_com,
	assujetti,age,categorie,sum(nombre) as nombre,sum(surface) as surface	
	from table_finale_tertiaire b_0 
		inner join t_dep c
			on b_0.idcom=c.code_insee 
	group by maille,c.dep_com,assujetti,age,categorie;
 
  	drop table if exists t_bat_3;
	create temporary table t_bat_3
	as select 'REGION' as maille, 
	assujetti,age,categorie,sum(nombre) as nombre,sum(surface) as surface
	from table_finale_tertiaire b_t 
	group by maille,assujetti,age,categorie;
	
	insert into table_finale_tertiaire
	select maille,NULL as idcom,siren_epci,NULL as dep_com,assujetti,age,categorie,nombre,surface 
	from t_bat_1;

	insert into table_finale_tertiaire
	select maille,NULL as idcom,NULL as siren_epci,dep_com,assujetti,age,categorie,nombre,surface
	from t_bat_2;

	insert into table_finale_tertiaire
	select maille,NULL as idcom,NULL as siren_epci,NULL as dep_com,assujetti,age,categorie,nombre,surface
	from t_bat_3;
	
	drop table if exists t_bat_a_1;
	create temporary table t_bat_a_1
	as select 'EPCI' as maille, c.siren_epci as siren_epci,
	categorie_surface,sum(nombre) as nombre,sum(surface) as surface
	from table_finale_tertiaire_a  b_0 
		left join t_communes c
			on b_0.idcom=c.code_insee
	group by maille,c.siren_epci,categorie_surface;
 
 	drop table if exists t_bat_a_2;
	create temporary table t_bat_a_2
	as select 'DEP' as maille, c.dep_com as dep_com,
	categorie_surface,sum(nombre) as nombre,sum(surface) as surface
	from table_finale_tertiaire_a b_0 
		inner join t_dep c
			on b_0.idcom=c.code_insee
	group by maille,c.dep_com,categorie_surface;
 
  	drop table if exists t_bat_a_3;
	create temporary table t_bat_a_3
	as select 'REGION' as maille, 
	categorie_surface,sum(nombre) as nombre,sum(surface) as surface
	from table_finale_tertiaire_a b_t 
	group by maille,categorie_surface;
	
	insert into table_finale_tertiaire_a
	select maille,NULL as idcom,siren_epci,NULL as dep_com,categorie_surface,nombre,surface
	from t_bat_a_1;

	insert into table_finale_tertiaire_a
	select maille,NULL as idcom,NULL as siren_epci,dep_com,categorie_surface,nombre,surface
	from t_bat_a_2;

	insert into table_finale_tertiaire_a
	select maille,NULL as idcom,NULL as siren_epci,NULL as dep_com,categorie_surface,nombre,surface
	from t_bat_a_3;
	
 	-- 4. SECRETISATION
	/*Tables de groupement communes et EPCI/EPT*/
		/*Communes et leurs EPCI hors département 28*/
		drop table if exists t_communes;
		create temporary table t_communes
		as select code_insee,siren_epci  -- EPCI
		from table_communes_insee
		where dep_com<>'28' and siren_epci is not null
		union
		select code_insee,id_ept as siren_epci -- EPT
		from table_communes_insee
		where dep_com<>'28' and id_ept is not null
		UNION
		SELECT code_insee,siren_epci 
		FROM table_communes_insee
		WHERE siren_epci='247800550'; -- CC du pays Houdanais, Région Ile de France et département 28

		drop table if exists t_dep;
		create temporary table t_dep
		as select code_insee,dep_com 
		from table_communes_insee
		where dep_com<>'28' 
		;
	/*table_finale_tertiaire*/
		/*Secret statistique si nombre concerné<11*/
		UPDATE table_finale_tertiaire
		SET secret_f = NULL;

		/*Secrétisation niveau 2 pour les EPCI/EPT*/	
		drop table if exists t_bat_2_s;
		create temporary table t_bat_2_s
		as
			select b_0.idcom, c.siren_epci,
			assujetti,age,categorie,
			nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_finale_tertiaire b_0 
				inner join t_communes c
					on b_0.idcom=c.code_insee
		order by siren_epci,assujetti,age,categorie,nombre asc
		;
		
		drop table if exists t_bat_2_s1;
		create temporary table t_bat_2_s1
		as
		select siren_epci,idcom,assujetti,age,categorie,nombre,secret_f_num,
		ROW_NUMBER() over(partition by siren_epci,assujetti,age,categorie order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by siren_epci,assujetti,age,categorie) as secret_f_sum
		from t_bat_2_s
		order by siren_epci,assujetti,age,categorie,nombre;
		
		/*Secrétisation niveau 2 pour les départements*/	
		drop table if exists t_bat_2_s2;
		create temporary table t_bat_2_s2
		as
			select b_0.idcom, c.dep_com,
			assujetti,age,categorie,nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_finale_tertiaire b_0 
				inner join t_dep c
					on b_0.idcom=c.code_insee
		order by dep_com,assujetti,age,categorie,nombre asc
		;
		
		drop table if exists t_bat_2_s3;
		create temporary table t_bat_2_s3
		as
		select dep_com,idcom,assujetti,age,categorie,nombre,secret_f_num,
		ROW_NUMBER() over(partition by dep_com,assujetti,age,categorie order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by dep_com,assujetti,age,categorie) as secret_f_sum
		from t_bat_2_s2;

		/*Secrétisation niveau 2 pour les sommes à la commune ou EPCI*/
		drop table if exists t_bat_2_s4;
		create temporary table t_bat_2_s4
		as
			select idcom,siren_epci,
			assujetti,age,categorie,
			nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_finale_tertiaire b_0 
		;		
		
		/*Secrétisation pour l'axe assujetti*/
		drop table if exists t_bat_2_s5;
		create temporary table t_bat_2_s5
		as
			select idcom,siren_epci,assujetti,age,categorie,nombre,
			ROW_NUMBER() over(partition by coalesce(idcom,siren_epci),assujetti order by nombre) as secret_f_rank,
			sum(secret_f_num) over(partition by coalesce(idcom,siren_epci),assujetti) as secret_f_sum
			from t_bat_2_s4
		;	
		
		/*Exemple
		select * from t_bat_2_s5  where siren_epci='200059889'
		*/
		
		/*Secrétisation pour l'axe categorie*/
		drop table if exists t_bat_2_s6;
		create temporary table t_bat_2_s6
		as
			select idcom,siren_epci,categorie,assujetti,age,
			ROW_NUMBER() over(partition by coalesce(idcom,siren_epci),categorie order by nombre) as secret_f_rank,
			sum(secret_f_num) over(partition by coalesce(idcom,siren_epci),categorie) as secret_f_sum
			from t_bat_2_s4
		;	
		
		/*Secrétisation pour l'axe categorie,age*/
		drop table if exists t_bat_2_s7;
		create temporary table t_bat_2_s7
		as
			select idcom,siren_epci,categorie,age,assujetti,
			ROW_NUMBER() over(partition by coalesce(idcom,siren_epci),categorie,age order by nombre) as secret_f_rank,
			sum(secret_f_num) over(partition by coalesce(idcom,siren_epci),categorie,age) as secret_f_sum
			from t_bat_2_s4
			where categorie in ('Bureau','Commerces')
		;
		
		/*Exemple:
		select * from t_bat_2_s7 where siren_epci='200017846'
		*/

		/*Union de tous les types de secrétisation*/
		drop table if exists t_bat_2_s8;
		create temporary table t_bat_2_s8
		as
		SELECT idcom,assujetti,age,categorie
		FROM t_bat_2_s1
		WHERE secret_f_sum=1 -- secret niveau 2 : Quand il n'y a qu'un seul élément, il faut secrétiser une deuxième commune
		and secret_f_rank = 2
		union
		SELECT idcom,assujetti,age,categorie
		FROM t_bat_2_s3
		WHERE secret_f_sum=1 -- secret niveau 2 : Quand il n'y a qu'un seul élément, il faut secrétiser une deuxième commune
		and secret_f_rank = 2
		union
		SELECT idcom,assujetti,age,categorie
		FROM t_bat_2_s5
		WHERE secret_f_sum=1 -- secret niveau 2 : Assujetti
		and secret_f_rank = 2
		and idcom is not null
		union
		SELECT idcom,assujetti,age,categorie
		FROM t_bat_2_s6
		WHERE secret_f_sum=1 -- secret niveau 2 : Categorie
		and secret_f_rank = 2		
		and idcom is not null
		union
		SELECT idcom,assujetti,age,categorie
		FROM t_bat_2_s7
		WHERE secret_f_sum=1 -- secret niveau 2 : Age
		and secret_f_rank = 2		
		and idcom is not null
		;

		update table_finale_tertiaire b
		set secret_f='2' 
		from t_bat_2_s8
		where b.idcom=t_bat_2_s8.idcom
								and b.assujetti=t_bat_2_s8.assujetti
								and b.age=t_bat_2_s8.age
								and b.categorie=t_bat_2_s8.categorie
								;
								
		drop table if exists t_bat_2_s9;
		create temporary table t_bat_2_s9
		as								
		SELECT siren_epci,assujetti,age,categorie
		FROM t_bat_2_s5
		WHERE secret_f_sum=1 -- secret niveau 2 : Assujetti
		and secret_f_rank = 2
		and siren_epci is not null
		union
		SELECT siren_epci,assujetti,age,categorie
		FROM t_bat_2_s6
		WHERE secret_f_sum=1 -- secret niveau 2 : Categorie
		and secret_f_rank = 2		
		and siren_epci is not null
		union
		SELECT siren_epci,assujetti,age,categorie
		FROM t_bat_2_s7
		WHERE secret_f_sum=1 -- secret niveau 2 : Age
		and secret_f_rank = 2		
		and siren_epci is not null
		;
			
		update table_finale_tertiaire b
		set secret_f='2' 
		from t_bat_2_s9
		where b.siren_epci=t_bat_2_s9.siren_epci
								and b.assujetti=t_bat_2_s9.assujetti
								and b.age=t_bat_2_s9.age
								and b.categorie=t_bat_2_s9.categorie
								;
								
		UPDATE table_finale_tertiaire
		SET secret_f='1'
		WHERE  	nombre<11;	-- secret de niveau 1, si le nombre de logements concernés est inférieur à 11 
		

	/*table_finale_tertiaire_a*/
		/*Secret statistique si nombre concerné<11*/
		UPDATE table_finale_tertiaire_a
		SET secret_f = NULL;

	
		/*Secrétisation niveau 2 pour les EPCI/EPT*/	
		drop table if exists t_bat_2_s;
		create temporary table t_bat_2_s
		as
			select b_0.idcom, c.siren_epci,
			categorie_surface,
			nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_finale_tertiaire_a b_0 
				inner join t_communes c
					on b_0.idcom=c.code_insee
		order by siren_epci,categorie_surface,nombre asc
		;
		
		drop table if exists t_bat_2_s1;
		create temporary table t_bat_2_s1
		as
		select siren_epci,idcom,categorie_surface,nombre,secret_f_num,
		ROW_NUMBER() over(partition by siren_epci,categorie_surface order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by siren_epci,categorie_surface) as secret_f_sum
		from t_bat_2_s
		order by siren_epci,categorie_surface,nombre;
		
		/*Secrétisation niveau 2 pour les départements*/	
		drop table if exists t_bat_2_s2;
		create temporary table t_bat_2_s2
		as
			select b_0.idcom, c.dep_com,
			categorie_surface,nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_finale_tertiaire_a b_0 
				inner join t_dep c
					on b_0.idcom=c.code_insee
		order by dep_com,categorie_surface,nombre asc
		;
		
		drop table if exists t_bat_2_s3;
		create temporary table t_bat_2_s3
		as
		select dep_com,idcom,categorie_surface,nombre,secret_f_num,
		ROW_NUMBER() over(partition by dep_com,categorie_surface order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by dep_com,categorie_surface) as secret_f_sum
		from t_bat_2_s2;

		/*Secrétisation niveau 2 par categorie de surface*/
		drop table if exists t_bat_ind;
		create temporary table t_bat_ind
		as
			select b_0.idcom, b_0.siren_epci,
			categorie_surface,nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_finale_tertiaire_a b_0 
		order by coalesce(b_0.idcom, b_0.siren_epci),categorie_surface
		;		
	
		drop table if exists t_bat_2_s4;
		create temporary table t_bat_2_s4
		as
		select idcom,siren_epci,categorie_surface,nombre,secret_f_num,
		ROW_NUMBER() over(partition by coalesce(idcom,siren_epci) order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by coalesce(idcom,siren_epci)) as secret_f_sum
		from t_bat_ind
		where categorie_surface<>'Inconnu';
		
		
		
		/* Exemple : select * from t_bat_2_s4 where siren_epci='249100553'*/
		
		/*Secrétisation EPCI, département, indicateur ter_1*/
		drop table if exists t_bat_2_s6;
		create temporary table t_bat_2_s6
		as
		SELECT idcom,categorie_surface
		FROM t_bat_2_s1
		WHERE secret_f_sum=1 -- secret niveau 2 : Quand il n'y a qu'un seul élément, il faut secrétiser une deuxième commune
		and secret_f_rank = 2
		union
		SELECT idcom,categorie_surface
		FROM t_bat_2_s3
		WHERE secret_f_sum=1 -- secret niveau 2 : Quand il n'y a qu'un seul élément, il faut secrétiser une deuxième commune
		and secret_f_rank = 2
		union
		SELECT idcom,categorie_surface
		FROM t_bat_2_s4
		WHERE secret_f_sum=1 -- secret niveau 2 : Quand il n'y a qu'un seul élément, il faut secrétiser une deuxième commune
		and secret_f_rank = 2
		and idcom is not null
		;

		drop table if exists t_bat_2_s7;
		create temporary table t_bat_2_s7
		as
		SELECT siren_epci,categorie_surface
		from t_bat_2_s4 -- secrétisation indicateur tertiaire
		where siren_epci is not null;
			
		update table_finale_tertiaire_a b
		set secret_f='2' 
		from t_bat_2_s6
		where b.idcom=t_bat_2_s6.idcom
								and b.categorie_surface=t_bat_2_s6.categorie_surface
								;
								
		update table_finale_tertiaire_a b
		set secret_f='2' 
		from t_bat_2_s7
		where b.siren_epci=t_bat_2_s7.siren_epci
								and b.categorie_surface=t_bat_2_s7.categorie_surface
								;

		UPDATE table_finale_tertiaire_a
		SET secret_f='1'
		WHERE  	nombre<11;	-- secret de niveau 1, si le nombre de locaux concernés est inférieur à 11 
		
/*
		-- 5. EXPORT
		select maille,idcom,siren_epci,dep_com,categorie_surface,
		case when secret_f in ('1','2') then NULL else nombre end as nombre,
		case when secret_f in ('1','2') then NULL else surface end as surface
		from table_finale_tertiaire_a

		select maille,idcom,siren_epci,dep_com,assujetti,age,categorie,
		case when secret_f in ('1','2') then NULL else nombre end as nombre,
		case when secret_f in ('1','2') then NULL else surface end as surface
		from table_finale_tertiaire
*/


END
$$

/*
SELECT * FROM t_b_ter_0 where idcom='78517' and categorie='Enseignement'
*/
